from glob import glob
import os

from kiwi.component import get_utility
from twisted.trial import unittest

from gazpacho.interfaces import IGazpachoApp

import common
from utils import diff_files

glade_dir = os.path.split(__file__)[0]
glade_files = glob(os.path.join(glade_dir, 'glade', '*.glade'))

class BaseTest(unittest.TestCase):
    def _test_gladefile(self, filename):
        tmp = filename + '.tmp'
        if os.path.exists(tmp):
            os.unlink(tmp)
        project = get_utility(IGazpachoApp).open_project(filename)
        project.save(tmp)

        value = diff_files(filename, tmp)

        if os.path.exists(tmp):
            os.unlink(tmp)

        if value:
            raise AssertionError("See diff output above")
DISABLED = {
    'messagedialog' : 'waiting for better Gtk+ support',
}

SKIPPED = {
    'frame': 'label widget packing properties lost',
    }

namespace = {}
for glade_file in glade_files:
    testname = os.path.basename(glade_file)[:-6]
    if testname in DISABLED:
        continue
    func = lambda self, f=glade_file: self._test_gladefile(f)
    name = 'test_%s' % testname
    func.__name__ = name
    if testname in SKIPPED:
        func.skip = SKIPPED[testname]
    namespace[name] = func
GladeTest = type('GladeTest', (BaseTest, object),
                 namespace)

