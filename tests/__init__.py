
# monkey patch gettext, improves performance significantly
# It must be done here, before anything of gazpacho is imported
import gettext
gettext.gettext = str

from gazpacho.main import check_deps
check_deps()

from gazpacho.catalog import load_catalogs
load_catalogs(external=False)
