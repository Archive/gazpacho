# Copyright (C) 2006 by Nokia Corporation
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import os
import os.path
import shutil
import tempfile

from twisted.trial import unittest

from gazpacho.plugins import Plugin, PluginInfo, PluginInfoError, PluginManager

def writePluginInfo(data):
    filename = tempfile.mktemp() + '.plugin'
    open(filename, 'w').write(data)
    return filename

class PluginInfoTest(unittest.TestCase):

    def testBasic(self):
        data = """[Gazpacho Plugin]
name = test
title = test title
class = test.Test
description = test description
author = test author
version = 0.1
"""

        f = writePluginInfo(data)
        pi = PluginInfo(f)
        os.unlink(f)

        self.assertEqual(pi.name, "test")
        self.assertEqual(pi.title, "test title")
        self.assertEqual(pi.class_name, "test.Test")
        self.assertEqual(pi.description, "test description")
        self.assertEqual(pi.author, "test author")
        self.assertEqual(pi.version, "0.1")

    def testBadSection(self):
        f = writePluginInfo("")
        self.assertRaises(PluginInfoError, PluginInfo, f)
        os.unlink(f)

        f = writePluginInfo("[Gazpacho]")
        self.assertRaises(PluginInfoError, PluginInfo, f)
        os.unlink(f)

        f = writePluginInfo("[Plugin]")
        self.assertRaises(PluginInfoError, PluginInfo, f)
        os.unlink(f)

    def testNoName(self):
        data = """[Gazpacho Plugin]
title = test title
class = test.Test
"""
        f = writePluginInfo(data)
        self.assertRaises(PluginInfoError, PluginInfo, f)
        os.unlink(f)

    def testNoTitle(self):
        # if a title is not provided, the name is used
        data = """[Gazpacho Plugin]
name = test
class = test.Test
"""
        f = writePluginInfo(data)
        pi = PluginInfo(f)
        os.unlink(f)

        self.assertEqual(pi.title, "test")

    def testNoClass(self):
        data = """[Gazpacho Plugin]
name = test
"""
        f = writePluginInfo(data)
        self.assertRaises(PluginInfoError, PluginInfo, f)
        os.unlink(f)

class FakeApp(object):
    pass

class Plugin1(Plugin):
    pass

class PluginManagerTest(unittest.TestCase):

    def testLoadPluginsDir(self):
        # create a temp layout for plugins
        plugin_dir = tempfile.mkdtemp()

        dir1 = os.path.join(plugin_dir, 'plugin1')
        os.mkdir(dir1)

        meta1 = """[Gazpacho Plugin]
name = plugin1
title = plugin1 title
class = test.Test
description = plugin1 description
author = plugin1 author
version = 0.1
"""
        file(os.path.join(dir1, 'plugin1.plugin'), 'w').write(meta1)

        pm = PluginManager()
        pm.load_plugins_dir(plugin_dir)

        shutil.rmtree(plugin_dir)

        plugins = pm.get_plugins()
        self.assertEqual(len(plugins), 1)

        p1 = plugins[0]
        self.assertEqual(p1.name, 'plugin1')
        self.assertEqual(p1.title, 'plugin1 title')
        self.assertEqual(p1.class_name, 'test.Test')
        self.assertEqual(p1.description, 'plugin1 description')
        self.assertEqual(p1.author, 'plugin1 author')
        self.assertEqual(p1.version, '0.1')

    def testActivatePlugin(self):
        pm = PluginManager()

        # create a PluginInfo object and add it to the manager manually
        data = """[Gazpacho Plugin]
name = test
title = test title
class = %s.Plugin1
description = test description
author = test author
version = 0.1
""" % __name__

        f = writePluginInfo(data)
        pi = PluginInfo(f)
        os.unlink(f)

        pm._plugins[pi.name] = pi

        self.assertEqual(pm.is_activated(pi.name), False)

        pm.activate_plugin(pi.name, FakeApp())
        self.assertEqual(pm.is_activated(pi.name), True)

        pm.deactivate_plugin(pi.name)
        self.assertEqual(pm.is_activated(pi.name), False)

        pm.activate_plugin(pi.name, FakeApp())
        self.assertEqual(pm.is_activated(pi.name), True)

        pm.deactivate_all()
        self.assertEqual(pm.is_activated(pi.name), False)

        self.assertRaises(PluginInfoError, pm.activate_plugin, 'foo',
                          FakeApp())
