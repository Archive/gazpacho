import os
import tempfile

from gazpacho.catalog import CustomUI
from gazpacho.properties import prop_registry

import common

class CustomUITest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)
        self._modified_types = []

    def tearDown(self):
        common.GazpachoTest.tearDown(self)
        # Nasty HACK. This is needed because after running a test of this
        # class the prop types are modified and so, tests in the same suite
        # that execute after this one will get a rotten prop type.
        # By removing it from the property storage it will be rebuild next
        # time it is acccesed
        for modified in self._modified_types:
            del prop_registry._normal_storage._types[modified]

    def _create_custom_ui_file(self, custom_ui_string):
        filename = tempfile.mktemp() + '.ui'
        open(filename, 'w').write(custom_ui_string)
        return filename

    def testCustomProperties(self):
        custom_ui = """<?xml version='1.0'?>
<gazpacho-custom-ui>
  <palette-group name='test' title='Test Widgets'>
    <widget name='GtkWindow'>
      <property name='title' default='Test'/>
      <property name='border-width' default='20'/>
      <property name='role' enabled='False'/>
      <property name='resizable' priority='1'/>
    </widget>
  </palette-group>
</gazpacho-custom-ui>"""
        self._modified_types += ['GtkWindow::title',
                                 'GtkContainer::border-width',
                                 'GtkWindow::role',
                                 'GtkWindow::resizable']
        filename = self._create_custom_ui_file(custom_ui)
        cui = CustomUI(filename)
        os.unlink(filename)

        # create a window
        window = self.create_gadget('GtkWindow')
        self.project.add_widget(window.widget)

        # Test it has the custom defaults
        self.assertEqual(window.widget.get_title(), 'Test')
        self.assertEqual(window.widget.get_border_width(), 20)

        # some properties are disabled
        self.assertEqual(window.get_prop('role').enabled, False)

        # some properties have very high priority
        self.assertEqual(window.get_prop('resizable').priority, 1)
