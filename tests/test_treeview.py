# Copyright (C) 2006 by Nokia Corporation
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import gobject
import gtk

import common

from gazpacho import gapi
from gazpacho.commandmanager import command_manager
from gazpacho.model import GazpachoModel
from gazpacho.widgets.base.treeview import GazpachoTreeViewColumn
from gazpacho.widgetregistry import widget_registry
from gazpacho.placeholder import Placeholder
from gazpacho.gadget import Gadget
from gazpacho.clipboard import clipboard

class GtkTreeViewTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        self.mgr = command_manager

    def _createTreeView(self):
        return gapi.create_gadget(self.project,
                                  widget_registry.get_by_name('GtkTreeView'),
                                  self.window.widget.get_child(),
                                  self.window, interactive=False)

    def _assertStore(self, store, size, types=tuple()):
        self.assertEqual(store.get_n_columns(), size)
        for index, t in enumerate(types):
            self.assertEqual(gobject.type_is_a(store.get_column_type(index),
                                               t),
                             True)
    def testCreation(self):
        # create a tree view
        tree_view = self._createTreeView()

        # it should have an adder column
        columns = tree_view.widget.get_columns()
        self.assertEqual(len(columns), 1)

        self.assertEqual(isinstance(columns[0], gtk.TreeViewColumn), True)
        self.assertEqual(isinstance(columns[0], GazpachoTreeViewColumn), False)

        # a model was added to the project
        model = self.project.model_manager.get_model('model1')
        store = model.get_model()
        self.assertEqual(isinstance(store, gtk.ListStore), True)
        # this initial model should have a (fake) column
        self._assertStore(store, 1, (gobject.TYPE_STRING,))

        # but the model will tell us it has no types yet
        # (because no columns were added)
        self.assertEqual(model.get_types(), [])

        # the model is the same as the tree_view model
        tree_model = tree_view.widget.get_model()
        self.assertEqual(model, GazpachoModel.from_model(tree_model))
        self.assertEqual(store, tree_model)

    def testAddColumn(self):
        # let's create a tree_view
        tree_view = self._createTreeView()

        # add a column
        column = tree_view.adaptor.add_tree_view_column(tree_view, False)

        # the colum has no renderers
        self.assertEqual(column.widget.get_cell_renderers(), [])

        model = self.project.model_manager.get_model('model1')
        m = GazpachoModel.from_model(tree_view.widget.get_model())
        self.assertEqual(m, model)

        # now we'll add a renderer
        renderer = gtk.CellRendererText()
        column.adaptor.create_layout(column.widget, ((str, renderer),))

        self.assertEqual(column.widget.cell_renderers[0], renderer)
        self.assertEqual(column.widget.get_type_for_cell_renderer(renderer),
                         str)
        self.assertEqual(column.widget.attributes[renderer], {})

        # tell the treeview we have renderers
        tree_view.adaptor.update_model_with_column(tree_view, column)

        store = model.get_model()
        self.assertEqual(isinstance(store, gtk.ListStore), True)
        self._assertStore(store, 2, (gobject.TYPE_STRING, gobject.TYPE_STRING))

        self.assertEqual(len(model.get_types()), 1)
        self.assertEqual(model.get_types()[0], str)

        # the column has updated its attributes
        self.assertEqual(column.widget.attributes[renderer], {'text': 0})

    def testAddColumnWithSeveralRenderers(self):
        tree_view = self._createTreeView()

        column = tree_view.adaptor.add_tree_view_column(tree_view, False)

        # add several renderers
        renderers = ((str, gtk.CellRendererText()),
                     (bool, gtk.CellRendererToggle()),
                     (gtk.gdk.Pixbuf, gtk.CellRendererPixbuf()))

        column.adaptor.create_layout(column.widget, renderers)
        for i, (t, cell) in enumerate(renderers):
            self.assertEqual(column.widget.cell_renderers[i], cell)
            self.assertEqual(column.widget.get_type_for_cell_renderer(cell), t)
            self.assertEqual(column.widget.attributes[cell], {})

        # update the model
        tree_view.adaptor.update_model_with_column(tree_view, column)
        model = GazpachoModel.from_model(tree_view.widget.get_model())

        store = model.get_model()
        self.assertEqual(isinstance(store, gtk.ListStore), True)
        self._assertStore(store, 4, (gobject.TYPE_STRING, gobject.TYPE_BOOLEAN,
                                     gtk.gdk.Pixbuf, gobject.TYPE_STRING))

        self.assertEqual(len(model.get_types()), 3)
        for i, (t, cell) in enumerate(renderers):
            self.assertEqual(model.get_types()[i], t)

        # the column has updated its attributes
        r = [r[1] for r in renderers]
        self.assertEqual(column.widget.attributes[r[0]], {'text': 0})
        self.assertEqual(column.widget.attributes[r[1]], {'active': 1})
        self.assertEqual(column.widget.attributes[r[2]], {'pixbuf': 2})

    def testAddSeveralColumns(self):
        tree_view = self._createTreeView()

        # column 1 (text)
        col1 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren1 = gtk.CellRendererText()
        col1.adaptor.create_layout(col1.widget, ((str, ren1),))
        tree_view.adaptor.update_model_with_column(tree_view, col1)

        # column 2 (check and text)
        col2 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren2_1 = gtk.CellRendererToggle()
        ren2_2 = gtk.CellRendererText()
        col2.adaptor.create_layout(col2.widget, ((bool, ren2_1), (str, ren2_2)))
        tree_view.adaptor.update_model_with_column(tree_view, col2)

        # column 3 (image and text)
        col3 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren3_1 = gtk.CellRendererPixbuf()
        ren3_2 = gtk.CellRendererText()
        col3.adaptor.create_layout(col3.widget, ((gtk.gdk.Pixbuf, ren3_1),
                                                 (str, ren3_2)))
        tree_view.adaptor.update_model_with_column(tree_view, col3)

        model = GazpachoModel.from_model(tree_view.widget.get_model())
        store = model.get_model()
        # check we have the right number of columns
        self.assertEqual(len(model.get_types()), 1+2+2)

        # check that the store is ok
        self._assertStore(store, 1+2+2+1, (gobject.TYPE_STRING,
                                           gobject.TYPE_BOOLEAN,
                                           gobject.TYPE_STRING,
                                           gtk.gdk.Pixbuf,
                                           gobject.TYPE_STRING))

        # check that the attributes are ok
        self.assertEqual(col1.widget.attributes[ren1], {'text': 0})
        self.assertEqual(col2.widget.attributes[ren2_1], {'active': 1})
        self.assertEqual(col2.widget.attributes[ren2_2], {'text': 2})
        self.assertEqual(col3.widget.attributes[ren3_1], {'pixbuf': 3})
        self.assertEqual(col3.widget.attributes[ren3_2], {'text': 4})

    def testChangeColumnLayout(self):
        tree_view = self._createTreeView()
        column = tree_view.adaptor.add_tree_view_column(tree_view, False)

        # add several renderers
        renderer = gtk.CellRendererText()

        column.adaptor.create_layout(column.widget, ((str, renderer),))

        tree_view.adaptor.update_model_with_column(tree_view, column)

        model = GazpachoModel.from_model(tree_view.widget.get_model())
        store = model.get_model()
        self.assertEqual(len(model.get_types()), 1)
        self._assertStore(store, 2, (gobject.TYPE_STRING, gobject.TYPE_STRING))
        self.assertEqual(column.widget.attributes[renderer], {'text': 0})

        # now let's change the layout
        tree_view.adaptor.update_model_with_column(tree_view, column, False)
        model2 = GazpachoModel.from_model(tree_view.widget.get_model())
        self.assertEqual(model is model2, True)
        store2 = model2.get_model()
        self.assertEqual(len(model2.get_types()), 0)
        self._assertStore(store2, 1, (gobject.TYPE_STRING,))

        new_renderer = gtk.CellRendererToggle()
        column.adaptor.create_layout(column.widget, ((bool, new_renderer),))

        tree_view.adaptor.update_model_with_column(tree_view, column, True)
        model3 = GazpachoModel.from_model(tree_view.widget.get_model())
        self.assertEqual(model is model3, True)
        store3 = model3.get_model()
        self.assertEqual(len(model3.get_types()), 1)
        self._assertStore(store3, 2, (gobject.TYPE_BOOLEAN,
                                      gobject.TYPE_STRING))
        self.assertEqual(column.widget.attributes[new_renderer], {'active': 0})

    def testChangeColumnLayout2(self):
        # change column layout when there are several columns. This is very
        # similar to next text
        # let's create a tree view with 3 columns
        tree_view = self._createTreeView()
        col1 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren1 = gtk.CellRendererText()
        col1.adaptor.create_layout(col1.widget, ((str, ren1),))
        tree_view.adaptor.update_model_with_column(tree_view, col1)

        col2 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren2_1 = gtk.CellRendererToggle()
        ren2_2 = gtk.CellRendererText()
        col2.adaptor.create_layout(col2.widget, ((bool, ren2_1), (str, ren2_2)))
        tree_view.adaptor.update_model_with_column(tree_view, col2)

        col3 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren3_1 = gtk.CellRendererPixbuf()
        col3.adaptor.create_layout(col3.widget, ((gtk.gdk.Pixbuf, ren3_1),))
        tree_view.adaptor.update_model_with_column(tree_view, col3)

        # now, we change the layout of the first column
        tree_view.adaptor.before_change_column_layout(tree_view, col1)

        new_renderer1 = gtk.CellRendererToggle()
        new_renderer2 = gtk.CellRendererText()
        col1.adaptor.create_layout(col1.widget, ((bool, new_renderer1),
                                                 (str, new_renderer2)))

        tree_view.adaptor.after_change_column_layout(tree_view, col1)

        # let's see if everything is ok
        model = GazpachoModel.from_model(tree_view.widget.get_model())
        store = model.get_model()
        self._assertStore(store, 6, (gobject.TYPE_BOOLEAN, gobject.TYPE_STRING,
                                     gobject.TYPE_BOOLEAN, gobject.TYPE_STRING,
                                     gtk.gdk.Pixbuf))
        self.assertEqual(col1.widget.attributes[new_renderer1], {'active': 0})
        self.assertEqual(col1.widget.attributes[new_renderer2], {'text': 1})
        self.assertEqual(col2.widget.attributes[ren2_1], {'active': 2})
        self.assertEqual(col2.widget.attributes[ren2_2], {'text': 3})
        self.assertEqual(col3.widget.attributes[ren3_1], {'pixbuf': 4})

    def testRemoveColumn(self):
        # things become harder when we have several columns and we remove one
        # that is not the last since all the other columns need to update their
        # attributes. Example:
        #
        # | col1   | col2   | col3   |
        # |text    |[] text | image  |
        #
        # If we remove col1 , col2 and col3 are 'shifted' to the left, so their
        # attribute mappings need to be updated

        # let's create a tree view with 3 columns
        tree_view = self._createTreeView()
        col1 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren1 = gtk.CellRendererText()
        col1.adaptor.create_layout(col1.widget, ((str, ren1),))
        tree_view.adaptor.update_model_with_column(tree_view, col1)

        col2 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren2_1 = gtk.CellRendererToggle()
        ren2_2 = gtk.CellRendererText()
        col2.adaptor.create_layout(col2.widget, ((bool, ren2_1), (str, ren2_2)))
        tree_view.adaptor.update_model_with_column(tree_view, col2)

        col3 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren3_1 = gtk.CellRendererPixbuf()
        col3.adaptor.create_layout(col3.widget, ((gtk.gdk.Pixbuf, ren3_1),))
        tree_view.adaptor.update_model_with_column(tree_view, col3)

        # this is the critical line
        gapi.delete_gadget(self.project, col1)

        # let's see if everything is ok
        model = GazpachoModel.from_model(tree_view.widget.get_model())
        store = model.get_model()
        self._assertStore(store, 4, (gobject.TYPE_BOOLEAN, gobject.TYPE_STRING,
                                     gtk.gdk.Pixbuf))
        self.assertEqual(col2.widget.attributes[ren2_1], {'active': 0})
        self.assertEqual(col2.widget.attributes[ren2_2], {'text': 1})
        self.assertEqual(col3.widget.attributes[ren3_1], {'pixbuf': 2})

    def testPreserveData(self):
        data = (('test-a1', 'test-b1'),
                ('test-a2', 'test-b2'),
                ('test-c1', 'test-c2'))
        tree_view = self._createTreeView()
        col1 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren1 = gtk.CellRendererText()
        col1.adaptor.create_layout(col1.widget, ((str, ren1),))
        tree_view.adaptor.update_model_with_column(tree_view, col1)

        model = GazpachoModel.from_model(tree_view.widget.get_model())
        store = model.get_model()
        for d in data:
            store.append((d[0], '')) # the empty string is for the fake column

        col2 = tree_view.adaptor.add_tree_view_column(tree_view, False)
        ren2 = gtk.CellRendererText()
        col2.adaptor.create_layout(col2.widget, ((str, ren2),))
        tree_view.adaptor.update_model_with_column(tree_view, col2)

        model = GazpachoModel.from_model(tree_view.widget.get_model())
        store = model.get_model()

        # check that the data is still there
        self.assertEqual(len(store), 3)
        for i, d in enumerate(data):
            self.assertEqual(store[i][0], d[0])


        # let's add the data for column 2
        for i, d in enumerate(data):
            store[i][1] = d[1]

        # remove col1
        gapi.delete_gadget(self.project, col1)

        model = GazpachoModel.from_model(tree_view.widget.get_model())
        store = model.get_model()
        # check that the data is still there (note that col2 data goes to
        # index 0)
        self.assertEqual(len(store), 3)
        for i, d in enumerate(data):
            self.assertEqual(store[i][0], d[1])


    def testDeletion(self):
        # create a tree view
        tree_view = self._createTreeView()

        # it should have an adder column
        columns = tree_view.widget.get_columns()
        self.assertEqual(len(columns), 1)

        self.assertEqual(isinstance(columns[0], gtk.TreeViewColumn), True)
        self.assertEqual(isinstance(columns[0], GazpachoTreeViewColumn), False)

        # a model was added to the project
        model = self.project.model_manager.get_model('model1')
        store = model.get_model()
        self.assertEqual(isinstance(store, gtk.ListStore), True)

        tree_view.deleted = True
        assert not self.project.model_manager.get_models()

        # Add the gadget dependencies
        tree_view.deleted = False
        assert self.project.model_manager.get_model('model1')

    def testDeletionRecursive(self):
        # create a tree view
        tree_view = self._createTreeView()

        # it should have an adder column
        columns = tree_view.widget.get_columns()
        self.assertEqual(len(columns), 1)

        self.assertEqual(isinstance(columns[0], gtk.TreeViewColumn), True)
        self.assertEqual(isinstance(columns[0], GazpachoTreeViewColumn), False)

        # a model was added to the project
        model = self.project.model_manager.get_model('model1')
        store = model.get_model()
        self.assertEqual(isinstance(store, gtk.ListStore), True)


        self.window.deleted = True
        assert not self.project.model_manager.get_models()

        # Add the gadget dependencies
        self.window.deleted = False
        assert self.project.model_manager.get_model('model1')


    def testCutPaste(self):        
        # create a tree view
        tree_view = self._createTreeView()
        name = tree_view.name
        assert tree_view

        clipboard.cut(tree_view)
        pl = self.window.widget.get_child()
        assert not self.project.get_gadget_by_name(name)
        assert isinstance(pl, Placeholder), "%s is not Placeholder" % pl

        clipboard.paste(pl, self.project)
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, gtk.TreeView), "%s is not Treeview" % tmp
        assert self.project.get_gadget_by_name(name)
        self.assertEqual(tree_view.name, tmp.get_name())


    def testCutPasteParent(self):
        # Test that we handle indirect cut and paste when operation on
        # the parent
        
        # create a tree view
        tree_view = self._createTreeView()
        name = tree_view.name
        assert tree_view

        clipboard.cut(self.window)
        assert not self.project.get_gadget_by_name(name)

        clipboard.paste(None, self.project)
        assert self.project.get_gadget_by_name(name)
    testCutPasteParent.skip = "Crash when pasting the parent. See bug #407617"
