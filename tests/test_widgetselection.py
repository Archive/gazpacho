import common

from gazpacho.project import WidgetSelection
from gazpacho import util


class TestWidgetSelection(WidgetSelection):
    """
    This is a slightly modified WidgetSelection to make it easier to
    test signal stuff.
    """

    def __init__(self):
        self.emitted = False
        WidgetSelection.__init__(self)

    def selection_changed(self):
        self.emitted = True

class WidgetSelectionTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        self.selection = TestWidgetSelection()

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(4)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 4)
        ph1, ph2, ph3, ph4 = children

        self.placeholder4 = ph4

        # add label1
        self.label1 = self.add_child(self.vbox, 'GtkLabel', ph1)

        # add label2
        self.label2 = self.add_child(self.vbox, 'GtkLabel', ph2)

        # add label3
        self.label3 = self.add_child(self.vbox, 'GtkLabel', ph3)

    def _initSelection(self, widget):
        self.selection._selection.append(widget.widget)

    def testAdd(self):
        assert not self.selection.emitted

        # Add a widget
        self.selection.add(self.label1.widget, True)
        self.assertEqual(len(self.selection), 1)
        assert self.label1.widget in self.selection

        assert self.selection.emitted

        # Reset emitted flag
        self.selection.emitted = False

        # Add another widget
        self.selection.add(self.label2.widget, True)
        self.assertEqual(len(self.selection), 2)
        assert self.label2.widget in self.selection

        assert self.selection.emitted

        # Reset emitted flag
        self.selection.emitted = False

        # Add the same widget (should not happen)
        self.selection.add(self.label2.widget, True)
        self.assertEqual(len(self.selection), 2)

        assert not self.selection.emitted

    def testAddNoEmit(self):
        assert not self.selection.emitted

        # Add a widget
        self.selection.add(self.label1.widget, False)
        self.assertEqual(len(self.selection), 1)
        assert self.label1.widget in self.selection

        assert not self.selection.emitted


    def testClear(self):
        # Init data
        self._initSelection(self.label1)
        self._initSelection(self.label2)

        assert not self.selection.emitted
        self.assertEqual(len(self.selection), 2)

        # Clear selection
        self.selection.clear(True)
        assert not self.selection

        assert self.selection.emitted

        # Reset emitted flag
        self.selection.emitted = False

        # Clear again
        self.selection.clear(True)
        assert not self.selection
        assert not self.selection.emitted

    def testClearNoEmit(self):
        # Init data
        self._initSelection(self.label1)
        self._initSelection(self.label2)

        assert not self.selection.emitted
        self.assertEqual(len(self.selection), 2)

        # Clear selection
        self.selection.clear(False)
        assert not self.selection

        assert not self.selection.emitted

    def testSet(self):
        # Init data
        self._initSelection(self.label1)
        self._initSelection(self.label2)

        assert not self.selection.emitted
        self.assertEqual(len(self.selection), 2)

        # Set a widget
        self.selection.set(self.label3.widget, True)
        self.assertEqual(len(self.selection), 1)
        assert self.label3.widget in self.selection

        assert self.selection.emitted

        # XXX isn't this how it should work?
        ## Reset emitted flag
        #self.selection.emitted = False
        #
        ## Init data
        #self._initSelection(self.label1)
        #self._initSelection(self.label2)
        #
        #self.selection.set(self.label1.widget, True)
        #self.assertEqual(len(self.selection), 1)
        #assert self.label1.widget in self.selection
        #
        #assert self.selection.emitted

    def testSetNoEmit(self):
        # Init data
        self._initSelection(self.label1)
        self._initSelection(self.label2)

        assert not self.selection.emitted
        self.assertEqual(len(self.selection), 2)

        # Set a widget
        self.selection.set(self.label3.widget, False)
        self.assertEqual(len(self.selection), 1)
        assert self.label3.widget in self.selection

        assert not self.selection.emitted

    def testRemove(self):
        # Init data
        self._initSelection(self.label1)
        self._initSelection(self.label2)
        self._initSelection(self.label3)

        assert not self.selection.emitted
        self.assertEqual(len(self.selection), 3)

        # Set a widget
        self.selection.remove(self.label2.widget, True)
        self.assertEqual(len(self.selection), 2)
        assert self.label1.widget in self.selection
        assert self.label3.widget in self.selection

        assert self.selection.emitted

        # Reset emitted flag
        self.selection.emitted = False

    def testRemoveNoEmit(self):
        # Init data
        self._initSelection(self.label1)
        self._initSelection(self.label2)
        self._initSelection(self.label3)

        assert not self.selection.emitted
        self.assertEqual(len(self.selection), 3)

        # Set a widget
        self.selection.remove(self.label2.widget, False)
        self.assertEqual(len(self.selection), 2)
        assert self.label1.widget in self.selection
        assert self.label3.widget in self.selection

        assert not self.selection.emitted

    def testSelectionChanged(self):
        assert not self.selection.emitted

        # Emit the signal
        self.selection.selection_changed()
        assert self.selection.emitted

    def testIter(self):
        # Init data
        widgets = [self.label1, self.label2, self.label3]
        for widget in widgets:
            self._initSelection(widget)

        for i, test in enumerate(self.selection):
            self.assertEqual(widgets[i].widget, test)

    def testLen(self):
        self.assertEqual(len(self.selection), 0)
        assert not self.selection

        # Add data
        self._initSelection(self.label1)
        self._initSelection(self.label2)

        self.assertEqual(len(self.selection), 2)
        assert self.selection

    def testContains(self):
        assert self.label1.widget not in self.selection

        # Add data
        self._initSelection(self.label1)
        self._initSelection(self.label2)

        assert self.label1.widget in self.selection
        assert self.label2.widget in self.selection

    def testGetItem(self):
        # Add data
        self._initSelection(self.label1)
        self._initSelection(self.label2)

        self.assertEqual(self.label1.widget, self.selection[0])
        self.assertEqual(self.label2.widget, self.selection[1])

    def testToggle(self):
        assert not self.selection.emitted

        # Add a widget
        self.selection.toggle(self.label1.widget, True)
        self.assertEqual(len(self.selection), 1)
        assert self.label1.widget in self.selection
        assert self.selection.emitted

        # Reset emitted flag
        self.selection.emitted = False

        # Add another widget
        self.selection.toggle(self.label1.widget, True)
        assert not self.selection
        assert self.selection.emitted

    def testToggleNoEmit(self):
        assert not self.selection.emitted

        # Add a widget
        self.selection.toggle(self.label1.widget, False)
        self.assertEqual(len(self.selection), 1)
        assert self.label1.widget in self.selection
        assert not self.selection.emitted

        # Reset emitted flag
        self.selection.emitted = False

        # Add another widget
        self.selection.toggle(self.label1.widget, False)
        assert not self.selection
        assert not self.selection.emitted

    def testCircle(self):
        # Test normal widget
        self.selection.circle(self.label1.widget)
        self.assertEqual(len(self.selection), 1)
        assert self.label1.widget in self.selection

        self.selection.circle(self.label1.widget)
        self.assertEqual(len(self.selection), 1)
        assert self.vbox.widget in self.selection

        self.selection.circle(self.label1.widget)
        self.assertEqual(len(self.selection), 1)
        assert self.window.widget in self.selection

        self.selection.circle(self.label1.widget)
        self.assertEqual(len(self.selection), 1)
        assert self.label1.widget in self.selection

        # Test normal placeholder
        self.selection.circle(self.placeholder4)
        self.assertEqual(len(self.selection), 1)
        assert self.placeholder4 in self.selection

        self.selection.circle(self.placeholder4)
        self.assertEqual(len(self.selection), 1)
        assert self.vbox.widget in self.selection

        self.selection.circle(self.placeholder4)
        self.assertEqual(len(self.selection), 1)
        assert self.window.widget in self.selection

        self.selection.circle(self.placeholder4)
        self.assertEqual(len(self.selection), 1)
        assert self.placeholder4 in self.selection

