import gtk
import gobject
import unittest

from gazpacho.command import CommandCreateDelete
from gazpacho.commandmanager import command_manager
from gazpacho.clipboard import clipboard
from gazpacho.dndhandlers import CommandDragDrop
from gazpacho.properties import prop_registry, PropType, PropRegistry
from gazpacho.properties import PropertyError, CommandSetProperty
from gazpacho.widgetadaptor import WidgetAdaptor

from common import GazpachoTest

class PropertyTest(GazpachoTest):
    def testBox(self):
        proj = self.open_buffer('''<glade-interface>
        <widget class="GtkWindow" id="window">
           <child>
              <widget class="GtkVBox" id="vbox">
                 <child>
                    <widget class="GtkButton" id="button"/>
                 </child>
              </widget>
           </child>
        </widget>
        </glade-interface>''')

        button = proj.get_gadget_by_name('button')
        self.failUnless(isinstance(button.widget, gtk.Button))
        expand = button.get_child_prop('expand')
        expand.value = False
        self.failIf(expand.value)

    def testTable(self):
        proj = self.open_buffer('''<glade-interface>
        <widget class="GtkWindow" id="window">
           <child>
              <widget class="GtkTable" id="table">
                 <child>
                    <widget class="GtkButton" id="button-1"/>
                 </child>
                 <child>
                    <widget class="GtkButton" id="button-2"/>
                 </child>
              </widget>
           </child>
        </widget>
        </glade-interface>''')

        button1 = proj.get_gadget_by_name('button-1')
        self.failUnless(isinstance(button1.widget, gtk.Button))

        prop = button1.get_child_prop('left-attach')
        prop.value = 10
        self.assertEqual(prop.value, 10)


class PropTypeTest(GazpachoTest):
    def setUp(self):
        GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testReparentWidget(self):
        # Test to reproduce Bug 314111. When a widget is moved from
        # one container to another the parent widget in the property
        # isn't updated to reflect this.

        # Add a VBox
        vbox = self.add_child(self.window, 'GtkVBox',
                                     self.window.widget.get_child())
        self.project.add_widget(vbox.widget)

        # Add a HBox and a Label to the VBox
        children = vbox.widget.get_children()
        assert len(children) == 3
        p0, p1, p2 = children

        hbox = self.add_child(vbox, 'GtkHBox', p0)
        self.project.add_widget(hbox.widget)

        label = self.add_child(vbox, 'GtkLabel', p1)
        self.project.add_widget(label.widget)

        # Move the Label from the VBox to the HBox
        placeholder = hbox.widget.get_children()[0]
        cmd = CommandDragDrop(label, placeholder)
        command_manager.execute(cmd, self.project)

    def testFloat(self):
        curve = self.create_gadget('GtkFrame')
        prop = curve.get_prop('label-xalign')
        prop.set(0.1)
        self.assertEqual(prop.save(), '0.1')
        prop.set(1.0)
        self.assertEqual(prop.save(), '1.0')

class MyLabel(gtk.Label):
    """Dummy widget used in PropTypeTest.testOverrideSimple"""

gobject.type_register(MyLabel)

def getGTypeFromBase(base):
    return getattr(gobject, 'TYPE_%s' % base.upper())

class PropertyRegistryTest(unittest.TestCase):
    def _createBaseTypes(self):
        self._bases = {}
        bases = ('Boolean', 'Pointer', 'String', 'Object', 'Int', 'UInt',
                 'Float', 'Double', 'Enum', 'Flags', 'Boxed', 'Char')
        for b in bases:
            self._bases[b] = type('My%sType' % b, (PropType,), {})

    def setUp(self):
        self.pr = PropRegistry()
        self._createBaseTypes()

    def testAddBaseType(self):
        self.assertEqual(self.pr._bases, {})
        MyBooleanType = self._bases['Boolean']
        gtype = getGTypeFromBase('Boolean')
        self.pr.add_base_type(MyBooleanType, gtype)

        self.assertEqual(self.pr._bases[gobject.TYPE_BOOLEAN], MyBooleanType)
        self.assertEqual(MyBooleanType.base_type, gobject.TYPE_BOOLEAN)

        self.assertRaises(PropertyError, self.pr.add_base_type,
                          MyBooleanType, gobject.TYPE_BOOLEAN)

    def testSubclassProperty(self):
        """In this test we are going to override a property changing its
        default value"""
        for name, klass in self._bases.items():
            self.pr.add_base_type(klass, getGTypeFromBase(name))

        class MyVisibleProp(self._bases['Boolean']):
            default = True

            def get_default(self, gobj):
                # raise an exception so we can check if the code flow reaches
                # this point
                raise AssertionError

        self.pr.override_simple('GtkWidget::visible', base=MyVisibleProp)

        v = [p for p in self.pr.list('GtkWidget') if p.name == 'visible'][0]
        self.assertEqual(v.default, True)

        # extra work to instantiate a property
        class FakeAdaptor(WidgetAdaptor):
            _default = gtk.Button()
            def __init__(self):
                pass

        class FakeGadget:
            project = None
            internal_name = None
            maintain_gtk_properties = False
            adaptor = FakeAdaptor()
            def __init__(self, widget):
                self.widget = widget

        button = gtk.Button()
        # when creating a prop, the 'load' method is called and eventually
        # the get_default method of our property class is called
        self.assertRaises(AssertionError, v, FakeGadget(button))

    def testOverrideSimple(self):
        """Test to demonstrate bug #330775:

        When calling override_simple on a widget subclass, the parent class
        get messed up
        """
        base = prop_registry.get_prop_type('GtkLabel', 'angle')
        prop_registry.override_simple('tests+test_properties+MyLabel::angle',
                                      base=base, editable=False)

        prop = prop_registry.get_prop_type('tests+test_properties+MyLabel',
                                           'angle')
        self.assertNotEqual(prop, base)


class ObjectPropertyTest(GazpachoTest):

    def setUp(self):
        GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testCutReference(self):
        # create a label with a entry as mnemonic widget
        box = self.add_child(self.window, 'GtkHBox',
                             self.window.widget.get_child())
        self.project.add_widget(box.widget)
        box.get_prop('size').set(2)

        ph1, ph2 = box.widget.get_children()
        label = self.add_child(box, 'GtkLabel', ph1)
        self.project.add_widget(label.widget)
        entry = self.add_child(box, 'GtkEntry', ph2)
        self.project.add_widget(entry.widget)

        prop = label.get_prop('mnemonic-widget')
        cmd = CommandSetProperty(prop, entry.widget)
        command_manager.execute(cmd, self.project)
        assert label.widget.get_mnemonic_widget() is entry.widget

        # test removing the entry (i.e. the reference target)
        clipboard.cut(entry)
        assert label.widget.get_mnemonic_widget() is None, "Reference should have been removed"

        # undo the removal
        command_manager.undo(self.project)
        assert label.widget.get_mnemonic_widget() is entry.widget, "Reference should have been restored"


    def testCutWithExternalReference(self):
        # create a label with a entry as mnemonic widget
        box = self.add_child(self.window, 'GtkHBox',
                             self.window.widget.get_child())
        self.project.add_widget(box.widget)
        box.get_prop('size').set(2)

        ph1, ph2 = box.widget.get_children()
        label = self.add_child(box, 'GtkLabel', ph1)
        self.project.add_widget(label.widget)
        entry = self.add_child(box, 'GtkEntry', ph2)
        self.project.add_widget(entry.widget)

        prop = label.get_prop('mnemonic-widget')
        cmd = CommandSetProperty(prop, entry.widget)
        command_manager.execute(cmd, self.project)
        assert label.widget.get_mnemonic_widget() is entry.widget

        # test removing the label (i.e. the reference target)
        clipboard.cut(label)
        p = box.widget.get_children()[0]
        test_gadget = clipboard.paste(p, self.project)
        assert test_gadget.widget.get_mnemonic_widget() is None, "Reference should have been removed"

        # undo the cut and paste
        command_manager.undo(self.project)
        command_manager.undo(self.project)
        assert label.widget.get_mnemonic_widget() is entry.widget, "Reference should still be there"

    def testDeleteReference(self):
        # create a label with a entry as mnemonic widget
        box = self.add_child(self.window, 'GtkHBox',
                             self.window.widget.get_child())
        self.project.add_widget(box.widget)
        box.get_prop('size').set(2)

        ph1, ph2 = box.widget.get_children()
        label = self.add_child(box, 'GtkLabel', ph1)
        self.project.add_widget(label.widget)
        entry = self.add_child(box, 'GtkEntry', ph2)
        self.project.add_widget(entry.widget)

        prop = label.get_prop('mnemonic-widget')
        cmd = CommandSetProperty(prop, entry.widget)
        command_manager.execute(cmd, self.project)
        assert label.widget.get_mnemonic_widget() is entry.widget

        # test removing the entry (i.e. the reference target)
        cmd = CommandCreateDelete(self.project, entry, None,
                                  entry.get_parent(), False)
        command_manager.execute(cmd, self.project)
        assert label.widget.get_mnemonic_widget() is None, "Reference should have been removed"

        # undo the removal
        command_manager.undo(self.project)
        assert label.widget.get_mnemonic_widget() is entry.widget, "Reference should have been restored"
