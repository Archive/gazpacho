import gtk

import common

from gazpacho.clipboard import clipboard
from gazpacho.commandmanager import command_manager
from gazpacho.gadget import Gadget
from gazpacho.widgets.base.button import CommandSetButtonContents

class WidgetTest(common.GazpachoTest):
    def setUp(self):
        common.GazpachoTest.setUp(self)
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testCopyName(self):
        "testCopyName - not enough, widgetview displays the same name"
        name = self.window.name

        # Test that the copy has a unique name
        window_copy = self.window.copy()
        self.assertEqual(self.window.name, name)
        self.assertNotEqual(window_copy.name, self.window.name)

        # Test copying without changing the name
        window_copy = self.window.copy(keep_name=True)
        self.assertEqual(self.window.name, name)
        self.assertEqual(window_copy.name, self.window.name)

    def testBoxCopyPaste(self):
        # add a box
        vbox1 = self.add_child(self.window, 'GtkVBox',
                               self.window.widget.get_child())
        self.project.add_widget(vbox1.widget)
        vbox1.get_prop('size').set(2)
        ph1, ph2 = vbox1.widget.get_children()

        # add another box
        vbox2 = self.add_child(vbox1, 'GtkVBox', ph1)
        self.assertEqual(vbox2.name, 'vbox2')
        self.project.add_widget(vbox2.widget)
        vbox2.get_prop('size').set(2)

        # Copy/paste
        clipboard.copy(vbox1)
        vbox3 = clipboard.paste(ph2, self.project)
        self.assertEqual(vbox3.name, 'vbox3')

        # Copied children, verify it exists and is the right name
        vbox4 = vbox3.widget.get_children()[0]
        self.failUnless(isinstance(vbox4, gtk.VBox))
        self.failUnless(Gadget.from_widget(vbox4))
        self.assertEqual(vbox4.name, 'vbox4')

    def testButtonCutPaste(self):
        self.button = self.add_child(self.window, 'GtkButton',
                                     self.window.widget.get_child())
        self.assertEqual(self.button.name, 'button1')
        self.project.add_widget(self.button.widget)
        cmd = CommandSetButtonContents(self.button, stock_id=gtk.STOCK_NEW,
                                       notext=True)
        command_manager.execute(cmd, self.project)

        image = self.button.widget.child
        self.failUnless(isinstance(image, gtk.Image))
        self.assertEqual(image.get_name(), 'image1')

        clipboard.cut(self.button)
        button = clipboard.paste(self.window.widget.get_child(),
                                   self.project)
        self.assertEqual(button.name, 'button1')

