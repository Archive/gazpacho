import common

import gtk

from gazpacho import gapi
from gazpacho.commandmanager import command_manager
from gazpacho.widgetregistry import widget_registry
from gazpacho.placeholder import Placeholder

class GtkScrolledWindowTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)


    def testUndoRemoveViewport(self):
        """To reproduce this bug:

        1 - Create scrolled window
        2 - Remove the viewport
        3 - Undo
        """
        sw = self.add_child(self.window, 'GtkScrolledWindow',
                            self.window.widget.get_child())
        self.project.add_widget(sw.widget)

        # when creating a scrolled window a viewport is automatically
        # created for us
        viewport = self.project.get_gadget_by_name('viewport1')
        self.assertEqual(isinstance(viewport.widget, gtk.Viewport), True)
        self.assertEqual(sw.widget.child, viewport.widget)

        # delete the viewport
        gapi.delete_gadget(self.project, viewport)

        command_manager.undo(self.project)

        self.assertEqual(sw.widget.child, viewport.widget)
    def testAddWidget(self):
        # Test adding a widget

        # init
        scrolled_gadget = self.add_child(self.window, 'GtkScrolledWindow',
                                  self.window.widget.get_child())
        scrolled = scrolled_gadget.widget
        self.project.add_widget(scrolled)
        viewport = scrolled.get_child()
        assert isinstance(viewport, gtk.Viewport)

        ph = viewport.get_child()
        assert isinstance(ph, Placeholder)

        # Add a label
        adaptor = widget_registry.get_by_name('GtkLabel')
        gapi.create_gadget(self.project, adaptor, ph, interactive=False)

        label = viewport.get_child()
        assert isinstance(label, gtk.Label)

        # Undo
        command_manager.undo(self.project)
        viewport = scrolled.get_child()
        assert isinstance(viewport, gtk.Viewport)

        ph = viewport.get_child()
        assert isinstance(ph, Placeholder)

        # Redo
        command_manager.redo(self.project)
        viewport = scrolled.get_child()
        assert viewport.get_child() is label

    def testAddWidgetWithViewport(self):
        # Test adding a widget with its own viewport
        # Test for #339960

        # init
        scrolled_gadget = self.add_child(self.window, 'GtkScrolledWindow',
                                  self.window.widget.get_child())
        scrolled = scrolled_gadget.widget
        self.project.add_widget(scrolled)
        viewport = scrolled.get_child()
        assert isinstance(viewport, gtk.Viewport)

        ph = viewport.get_child()
        assert isinstance(ph, Placeholder)

        # Add a label
        adaptor = widget_registry.get_by_name('GtkTextView')
        gapi.create_gadget(self.project, adaptor, ph, interactive=False)

        textview = scrolled.get_child()
        assert isinstance(textview, gtk.TextView)

        # Undo
        command_manager.undo(self.project)
        viewport = scrolled.get_child()
        assert isinstance(viewport, gtk.Viewport)

        ph = viewport.get_child()
        assert isinstance(ph, Placeholder)

        # Redo
        command_manager.redo(self.project)
        assert scrolled.get_child() is textview
    testAddWidgetWithViewport.skip = "Test for #339960 - Error when undoing adding a child"
