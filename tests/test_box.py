# Copyright (C) 2004,2005 by SICEm S.L. and Imendio AB
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import common
from gazpacho.commandmanager import command_manager
from gazpacho.propertyeditor import CommandSetProperty
from gazpacho.placeholder import Placeholder
from gazpacho.widgets.base.box import CommandBoxDeletePlaceholder, \
     CommandBoxInsertPlaceholder

class GtkBoxTest(common.GazpachoTest):
    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testExpand(self):
        """testExpand - Test for bug #313439

        Steps to reproduce:

          1. Create a Window
          2. Create a VBox
          3. Create another VBox inside the first vbox
          4. Select the window, then the inner vbox, then the outter box
        """

        parent_vbox = self.add_child(self.window, 'GtkVBox',
                                     self.window.widget.get_child())
        self.project.add_widget(parent_vbox.widget)

        placeholder = parent_vbox.widget.get_children()[0]
        child_vbox = self.add_child(parent_vbox, 'GtkVBox',
                                    placeholder)
        self.project.add_widget(child_vbox.widget)

        self.window.select()
        child_vbox.select()
        parent_vbox.select()

    def testSizeProperty(self):

        # add a box
        vbox = self.add_child(self.window, 'GtkVBox',
                              self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        vbox.get_prop('size').set(3)

        ph1, ph2, ph3 = vbox.widget.get_children()
        label1 = self.add_child(vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        label2 = self.add_child(vbox, 'GtkLabel', ph3)
        self.project.add_widget(label2.widget)

        # Make sure the placeholder is removed first
        vbox.get_prop('size').set(2)
        children = vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        assert ph2 not in children
        assert label1.widget in children
        assert label2.widget in children

        # Make sure the last widget is removed second
        vbox.get_prop('size').set(1)
        children = vbox.widget.get_children()
        self.assertEqual(len(children), 1)
        assert ph2 not in children
        assert label2.widget not in children
        assert label1.widget in children

    def testSizePropertyCommand(self):
        # Test for #330049

        # Init data
        vbox = self.add_child(self.window, 'GtkVBox',
                              self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        prop = vbox.get_prop('size')
        prop.set(3)

        ph1, ph2, ph3 = vbox.widget.get_children()
        label1 = self.add_child(vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        label2 = self.add_child(vbox, 'GtkLabel', ph3)
        self.project.add_widget(label2.widget)

        # Set size to 1, this will remove 2 widgets
        command_manager.execute(CommandSetProperty(prop, 1), self.project)
        children = vbox.widget.get_children()
        self.assertEqual(len(children), 1)
        assert label1.widget is children[0]

        # Undo and test that the widgets are restored
        command_manager.undo(self.project)
        children = vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        w1, w2, w3 = children
        assert label1.widget is w1
        assert label2.widget is w3
        assert ph2 is w2
    testSizePropertyCommand.skip = "Undo size prop change doesn't restore widgets. See #330049"

    def testBoxInsertPlaceholder(self):

        # add a box
        vbox = self.add_child(self.window, 'GtkVBox',
                              self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        vbox.get_prop('size').set(1)

        p = vbox.widget.get_children()[0]
        label = self.add_child(vbox, 'GtkLabel', p)

        # Insert after label
        command_manager.execute(CommandBoxInsertPlaceholder(vbox, 0, True),
                                self.project)

        children = vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        w, p = children
        assert label.widget is w
        assert isinstance(p, Placeholder)

        # Insert before label
        command_manager.execute(CommandBoxInsertPlaceholder(vbox, 0, False),
                                self.project)

        children = vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        p1, w, p2 = children
        assert label.widget is w
        assert isinstance(p1, Placeholder)
        assert isinstance(p2, Placeholder)

        # Undo everything
        command_manager.undo(self.project)
        command_manager.undo(self.project)

        children = vbox.widget.get_children()
        self.assertEqual(len(children), 1)
        assert label.widget is children[0]

    def testDelete(self):
        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(2)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph1, ph2 = children

        # Delete the placeholder
        command_manager.execute(CommandBoxDeletePlaceholder(ph1), self.project)
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 1)
        self.assertEqual(ph2, children[0])

        # Undo
        command_manager.undo(self.project)
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        self.assertEqual(ph1, children[0])
        self.assertEqual(ph2, children[1])

        # Redo
        command_manager.redo(self.project)
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 1)
        self.assertEqual(ph2, children[0])

