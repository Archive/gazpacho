# Copyright (C) 2004,2005 by SICEm S.L. and Imendio AB
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import gtk

from gazpacho.filewriter import XMLWriter
from gazpacho.project import GazpachoObjectBuilder
from gazpacho.commandmanager import command_manager
from gazpacho.widgets.base.button import CommandSetButtonContents

import common

class GtkButtonTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testCreation(self):
        button = self.add_child(self.window, 'GtkButton',
                                self.window.widget.get_child())

        self.assertEqual(gtk.Button, type(button.widget))

    def testProperty(self):
        button = self.add_child(self.window, 'GtkButton',
                                self.window.widget.get_child())

        prop = button.get_prop('relief')
        prop.set(gtk.RELIEF_HALF)
        self.assertEqual(button.widget.get_relief(), gtk.RELIEF_HALF)

    def testSavingLoading(self):
        button = self.add_child(self.window, 'GtkButton',
                                self.window.widget.get_child())
        self.project.add_widget(button.widget)
        self.project.save(__file__+'.glade')

        self.app.close_current_project()

        self.app.open_project(__file__+'.glade')
        self.remove_file(__file__+'.glade')

class ButtonSave(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)
        self.button = self.add_child(self.window, 'GtkButton',
                                     self.window.widget.get_child())

    def serialize(self):
        xw = XMLWriter(project=self.project)
        node = xw.serialize_node(self.button)
        self.properties = node.getElementsByTagName('property')
        self.project.save(__file__+'.glade')
        ob = GazpachoObjectBuilder(filename=__file__+'.glade')
        self.remove_file(__file__+'.glade')

        self.gbutton = ob.get_widget('button1')

    def testSaveLabel(self):
        prop = self.button.get_prop('label')
        prop.set('Simple')
        self.serialize()
        self.assertEqual(self.gbutton.get_label(), 'Simple')

    def testSaveLabelStock(self):
        self.button.get_prop('use-stock').set(True)
        self.button.get_prop('label').set('gtk-new')
        self.serialize()
        self.assertEqual(self.gbutton.get_label(), 'gtk-new')
        self.assertEqual(self.gbutton.get_use_stock(), True)

    def testSaveStock(self):
        self.button.get_prop('use-stock').set(True)
        self.serialize()
        self.assertEqual(self.gbutton.get_use_stock(), True)

class ButtonSave2(common.GazpachoTest):

    # Disabled, we do we really want to support this?
    def _testAdvanced(self):
        glade_data = '''<widget class="GtkButton" id="button1">
    <property name="receives_default">True</property>
    <property name="events"></property>
    <property name="can_focus">True</property>
    <child>
        <widget class="GtkAlignment" id="button1-alignment">
            <property name="visible">True</property>
            <property name="xalign">0.5</property>
            <property name="yalign">0.5</property>
            <property name="xscale">1.0</property>
            <property name="yscale">1.0</property>
            <child>
                <widget class="GtkHBox" id="button1-box">
                    <property name="visible">True</property>
                    <child>
                        <widget class="GtkImage" id="button1-image">
                            <property name="visible">True</property>
                            <property name="file">image.png</property>
                        </widget>
                    </child>
                    <child>
                        <widget class="GtkLabel" id="button1-label">
                            <property name="visible">True</property>
                            <property name="label">Details</property>
                        </widget>
                        <packing>
                            <property name="position">1</property>
                        </packing>
                    </child>
                </widget>
            </child>
        </widget>
    </child>
</widget>'''

        # XXX: crack api, fix, open_buffer iss probably better.
        project = self.project.open('', self.app, buffer=glade_data)
        self.failUnless(project.get_widgets() != [], 'no loaded widgets')
        button = project.get_gadget_by_name('button1')
        self.failUnless(button != None, 'missing button')
        gbutton = button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self.assertEqual(len(gbutton.get_children()), 1,
                         'missing child for button')
        child = gbutton.get_children()[0]
        self.assertEqual(isinstance(child, gtk.Alignment), True)

        xw = XMLWriter()
        node = xw.serialize_node(button)
        xw.write_node(__file__+'.glade', node)
        ob = GazpachoObjectBuilder(filename=__file__+'.glade')
        button = ob.get_widget('button1')
        self.failUnless(button != None, 'button not saved')
        self.assertEqual(isinstance(button, gtk.Button), True)
        self.assertEqual(len(button.get_children()), 1,
                         'alignment not saved')
        child = button.get_children()[0]
        self.failUnless(button != None, 'alignment not saved')
        self.assertEqual(isinstance(child, gtk.Alignment), True)

class CreateSave(common.GazpachoTest):
    def _create_button(self):
        window = self.create_gadget('GtkWindow')
        window.get_prop('visible').set(False)
        self.project.add_widget(window.widget)

        return self.add_child(window, 'GtkButton',
                              window.widget.get_child())
    def setUp(self):
        common.GazpachoTest.setUp(self)

        self.button = self._create_button()
        self.failUnless(self.button != None)

    def save_and_load(self):
        xw = XMLWriter(project=self.project)
        node = xw.serialize_node(self.button)
        xw.write_node(__file__+'.glade', node)
        ob = GazpachoObjectBuilder(filename=__file__+'.glade')
        button = ob.get_widget('button1')
        self.remove_file(__file__+'.glade')

        self.failUnless(button != None, 'button not saved')
        self.assertEqual(isinstance(button, gtk.Button), True)
        return button

    def _assertStockAndText(self, gbutton):
        self.assertEqual(self.button.get_prop('use-stock').value,
                         True, 'use-stock not property set')
        self.assertEqual(gbutton.get_use_stock(), True)
        self.assertEqual(len(gbutton.get_children()), 1,
                         'alignment not set')

    def testStockAndText(self):
        cmd = CommandSetButtonContents(self.button, stock_id=gtk.STOCK_NEW,
                                        notext=False)
        command_manager.execute(cmd, self.project)
        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertStockAndText(gbutton)

        button = self.save_and_load()

        self.assertEqual(button.get_label(), gtk.STOCK_NEW,
                         'label is not saved')
        self.assertEqual(button.get_use_stock(), True,
                         'stock is not saved')

    def testStockAndTextUndo(self):
        cmd = CommandSetButtonContents(self.button, stock_id=gtk.STOCK_NEW,
                                       notext=False)
        command_manager.execute(cmd, self.project)
        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertStockAndText(gbutton)

        command_manager.undo(self.project)
        command_manager.redo(self.project)

        self._assertStockAndText(gbutton)

    def _assertStockNoText(self, gbutton):
        self.assertEqual(len(gbutton.get_children()), 1,
                         'image not set')
        child = gbutton.get_child()
        self.failUnless(child != None)
        self.assertEqual(isinstance(child, gtk.Image), True)

    def testStockNoText(self):
        cmd = CommandSetButtonContents(self.button, stock_id=gtk.STOCK_NEW,
                                       notext=True)
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertStockNoText(gbutton)

        button = self.save_and_load()
        self.assertEqual(isinstance(button.get_child(), gtk.Image), True)

    def testStockNoTextUndo(self):
        cmd = CommandSetButtonContents(self.button, stock_id=gtk.STOCK_NEW,
                                       notext=True)
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertStockNoText(gbutton)

        command_manager.undo(self.project)
        command_manager.redo(self.project)

        self._assertStockNoText(gbutton)

    def _assertJustLabel(self, gbutton):
        self.assertEqual(len(gbutton.get_children()), 1, 'label not set')
        child = gbutton.get_child()
        self.failUnless(child != None)
        self.assertEqual(isinstance(child, gtk.Label), True)
        self.assertEqual(child.get_text(), 'Test')
        self.assertEqual(gbutton.get_label(), 'Test')

    def testJustLabel(self):
        cmd = CommandSetButtonContents(self.button, label='Test')
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertJustLabel(gbutton)

        button = self.save_and_load()
        self.assertEqual(isinstance(button.get_child(), gtk.Label), True)
        self.assertEqual(button.get_label(), 'Test')

    def testJustLabelUndo(self):
        cmd = CommandSetButtonContents(self.button, label='Test')
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertJustLabel(gbutton)

        command_manager.undo(self.project)
        command_manager.redo(self.project)

        self._assertJustLabel(gbutton)

    def _assertJustImage(self, gbutton):
        self.assertEqual(len(gbutton.get_children()), 1, 'image not set')
        child = gbutton.get_child()
        self.failUnless(child != None)
        self.assertEqual(isinstance(child, gtk.Image), True)

    def testJustImage(self):
        cmd = CommandSetButtonContents(self.button, image_path='/path/to/image')
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertJustImage(gbutton)

        button = self.save_and_load()
        self.assertEqual(isinstance(button.get_child(), gtk.Image), True)

    def testJustImageUndo(self):
        cmd = CommandSetButtonContents(self.button, image_path='/path/to/image')
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertJustImage(gbutton)

        command_manager.undo(self.project)
        command_manager.redo(self.project)

        self._assertJustImage(gbutton)

    def _assertLabelAndImage(self, gbutton, pos=gtk.POS_LEFT):
        self.assertEqual(len(gbutton.get_children()), 1)
        align = gbutton.get_child()
        self.failUnless(align != None)
        self.assertEqual(isinstance(align, gtk.Alignment), True)

        box = align.get_child()
        self.failUnless(box != None)
        if pos in [gtk.POS_TOP, gtk.POS_BOTTOM]:
            self.assertEqual(isinstance(box, gtk.VBox), True)
        else:
            self.assertEqual(isinstance(box, gtk.HBox), True)

        label, image = box.get_children()
        if pos in [gtk.POS_LEFT, gtk.POS_TOP]:
            image, label = label, image
        self.assertEqual(isinstance(image, gtk.Image), True)
        self.assertEqual(isinstance(label, gtk.Label), True)

        self.assertEqual(label.get_text(), 'Test')

    def testLabelAndImage(self):
        cmd = CommandSetButtonContents(self.button, label='Test',
                                       image_path='/path/to/image',
                                       position=gtk.POS_LEFT)
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertLabelAndImage(gbutton)

        self.save_and_load()
        self._assertLabelAndImage(gbutton)

    def testLabelAndImageUndo(self):
        cmd = CommandSetButtonContents(self.button, label='Test',
                                       image_path='/path/to/image',
                                       position=gtk.POS_LEFT)
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertLabelAndImage(gbutton, gtk.POS_LEFT)

        command_manager.undo(self.project)
        command_manager.redo(self.project)

        self._assertLabelAndImage(gbutton, gtk.POS_LEFT)

    def testLabelAndImagePositionUndo(self):
        # Test undo for all positions

        # Left
        cmd = CommandSetButtonContents(self.button, label='Test',
                                       image_path='/path/to/image',
                                       position=gtk.POS_LEFT)
        command_manager.execute(cmd, self.project)

        gbutton = self.button.widget
        self.assertEqual(isinstance(gbutton, gtk.Button), True)
        self._assertLabelAndImage(gbutton, gtk.POS_LEFT)

        # Right
        cmd = CommandSetButtonContents(self.button, label='Test',
                                       image_path='/path/to/image',
                                       position=gtk.POS_RIGHT)
        command_manager.execute(cmd, self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_RIGHT)

        # Top
        cmd = CommandSetButtonContents(self.button, label='Test',
                                       image_path='/path/to/image',
                                       position=gtk.POS_TOP)
        command_manager.execute(cmd, self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_TOP)

        # Bottom
        cmd = CommandSetButtonContents(self.button, label='Test',
                                       image_path='/path/to/image',
                                       position=gtk.POS_BOTTOM)
        command_manager.execute(cmd, self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_BOTTOM)

        # LEFT
        cmd = CommandSetButtonContents(self.button, label='Test',
                                       image_path='/path/to/image',
                                       position=gtk.POS_LEFT)
        command_manager.execute(cmd, self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_LEFT)


        command_manager.undo(self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_BOTTOM)

        command_manager.undo(self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_TOP)

        command_manager.undo(self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_RIGHT)

        command_manager.undo(self.project)
        self._assertLabelAndImage(gbutton, gtk.POS_LEFT)

    def testUniqueNames(self):
        cmd = CommandSetButtonContents(self.button, image_path='/path/to/image')
        command_manager.execute(cmd, self.project)

        self.button2 = self._create_button()
        cmd = CommandSetButtonContents(self.button2,
                                       image_path='/path/to/image/2')
        command_manager.execute(cmd, self.project)

        button = self.button.widget
        self.assertNotEqual(button.child.get_name(), 'GtkImage')

        button2 = self.button2.widget
        self.assertNotEqual(button2.child.get_name(), 'GtkImage')

        self.assertNotEqual(button.child.get_name(),
                            button2.child.get_name())
