import common

import gtk

from gazpacho.clipboard import clipboard, CommandCutPaste
from gazpacho.command import CommandCreateDelete
from gazpacho.commandmanager import command_manager
from gazpacho.dndhandlers import CommandDragDrop
from gazpacho.gadget import Gadget
from gazpacho.properties import CommandSetProperty

class GtkTableTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').value = False
        self.project.add_widget(self.window.widget)

    def testCreation(self):
        table = self.add_child(self.window, 'GtkTable',
                               self.window.widget.get_child())

        self.assertEqual(gtk.Table, type(table.widget))

    def testProperty(self):
        table = self.add_child(self.window, 'GtkTable',
                               self.window.widget.get_child())

        prop = table.get_prop('n-rows')
        prop.value = 10
        self.assertEqual(prop.value, 10)
        self.assertEqual(table.widget.get_property('n-rows'), 10)

        prop = table.get_prop('n-columns')
        prop.value = 20
        self.assertEqual(prop.value, 20)
        self.assertEqual(table.widget.get_property('n-columns'), 20)

    def testAddChildren(self):
        table = self.add_child(self.window, 'GtkTable',
                               self.window.widget.get_child())

        table.get_prop('n-rows').value = 4
        table.get_prop('n-columns').value = 4

        for placeholder in table.widget.get_children():
            button = self.add_child(table, 'GtkButton', placeholder)

        left_attach = button.get_child_prop('left-attach')

        for child in table.widget.get_children():
            self.assertEqual(gtk.Button, type(child))

    def testSavingLoading(self):
        table = self.add_child(self.window, 'GtkTable',
                               self.window.widget.get_child())
        self.project.add_widget(table.widget)
        self.project.save(__file__+'.glade')

        self.app.close_current_project()

        self.app.open_project(__file__+'.glade')
        self.remove_file(__file__+'.glade')


    def testOptionsProperty(self):
        # Test that cutting/pasting a table with the x/y-options
        # property set will produce the correct result

        table = self.add_child(self.window, 'GtkTable',
                               self.window.widget.get_child())
        self.project.add_widget(table.widget)

        table.get_prop('n-rows').value = 2
        table.get_prop('n-columns').value = 1
        placeholders = table.widget.get_children()
        assert len(placeholders) == 2

        # Add two labels and set different x-options for them
        label1 = self.add_child(table, 'GtkLabel', placeholders[0])
        self.project.add_widget(label1.widget)
        label2 = self.add_child(table, 'GtkLabel', placeholders[1])
        self.project.add_widget(label2.widget)

        label1.get_child_prop('x-options').value = 0
        label2.get_child_prop('x-options').value =  gtk.EXPAND | gtk.SHRINK | gtk.FILL

        # cut and paste
        clipboard.cut(table)
        clipboard.paste(self.window.widget.get_child(), self.project)

        children = self.window.widget.get_child().get_children()
        assert len(children) == 2

        # compare the x-options values with the original
        for child in children:
            gadget = Gadget.from_widget(child)
            value = gadget.get_child_prop('x-options').value
            if gadget.name == label1.name:
                self.assertEqual(value, 0)
            elif gadget.name == label2.name:
                self.assertEqual(value, gtk.EXPAND | gtk.SHRINK | gtk.FILL)

    def testUndoRedoMess(self):
        """testUndoRedoMess: See #347813 for steps to reproduce"""
        table = self.add_child(self.window, 'GtkTable',
                               self.window.widget.get_child())
        self.project.add_widget(table.widget)

        # add a label to the table
        label = self.create_gadget('GtkLabel')
        ph = table.widget.get_children()[0]
        cmd = CommandCreateDelete(self.project, label, ph, table, True)
        command_manager.execute(cmd, self.project)

        # change the 'x' property
        prop = label.get_child_prop('x-pos')
        cmd = CommandSetProperty(prop, 1)
        command_manager.execute(cmd, self.project)

        # drag the label back to the x-pos=0 position
        ph = table.widget.get_children()[0]
        cmd = CommandDragDrop(label, ph)
        command_manager.execute(cmd, self.project)

        # undo two times
        command_manager.undo(self.project)
        command_manager.undo(self.project)

        # redo two times
        command_manager.redo(self.project)
        command_manager.redo(self.project)

    testUndoRedoMess.skip = "Problems with undo/redo See bug #347813"

    def testCopyPasteMess(self):
        """testCopyPasteMess: See #347813 for steps to reproduce"""
        table = self.add_child(self.window, 'GtkTable',
                               self.window.widget.get_child())
        self.project.add_widget(table.widget)

        # add a label to the table
        label = self.create_gadget('GtkLabel')
        ph = table.widget.get_children()[0]
        cmd = CommandCreateDelete(self.project, label, ph, table, True)
        command_manager.execute(cmd, self.project)

        # change the 'x' property
        prop = label.get_child_prop('x-pos')
        cmd = CommandSetProperty(prop, 1)
        command_manager.execute(cmd, self.project)

        # copy/paste the label back to the x-pos=0 position
        ph = table.widget.get_children()[0]
        cmd = CommandCutPaste(label, label.project, ph, False)
        command_manager.execute(cmd, self.project)

        # undo two times
        command_manager.undo(self.project)
        command_manager.undo(self.project)

        # redo two times
        command_manager.redo(self.project)
        command_manager.redo(self.project)

    testCopyPasteMess.skip = "Problems with copy/paste See bug #347813"
