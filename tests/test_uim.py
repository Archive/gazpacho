import os
import xml.dom

import gtk

from gazpacho.clipboard import clipboard
from gazpacho.filewriter import XMLWriter
from gazpacho.placeholder import Placeholder
from gazpacho.project import GazpachoObjectBuilder
from gazpacho.gadget import Gadget, load_gadget_from_widget
from gazpacho.command import CommandCreateDelete

import common
from utils import diff_files

class FakeGadget(object):
    def __init__(self, name):
        self.name = name

class WidgetsCounter(object):
    def __init__(self, n):
        self.n = n

class UIMTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.project.add_widget(self.window.widget)

    def create_toolbar(self):
        placeholder = self.window.widget.get_child()
        toolbar = self.create_gadget('GtkToolbar')
        Gadget.replace(placeholder, toolbar.widget, self.window)
        self.project.add_widget(toolbar.widget)
        return toolbar

    def create_menubar(self):
        placeholder = self.window.widget.get_child()
        menubar = self.create_gadget('GtkMenuBar')
        Gadget.replace(placeholder, menubar.widget, self.window)
        self.project.add_widget(menubar.widget)
        return menubar

    def testCreateDefaultActions(self):
        self.project.uim.create_default_actions()

        # now we should have a bunch of actions inside
        # an action group in the uim
        action_groups = self.project.uim._uim.get_action_groups()
        self.assertEqual(len(action_groups), 1)

        ag = action_groups[0]

        self.assertEqual(ag.get_name(), 'DefaultActions')

        actions = ag.list_actions()

        # we should have 10 actions
        self.assertEqual(len(actions), 10)

    def testCreateDefaultActionsTwice(self):
        self.project.uim.create_default_actions()

        # add them again
        self.project.uim.create_default_actions()

        # we should only have one action group and 10 actions
        action_groups = self.project.uim._uim.get_action_groups()
        self.assertEqual(len(action_groups), 1)
        actions = action_groups[0].list_actions()
        self.assertEqual(len(actions), 10)

    def testRemoveActionGroup(self):
        self.project.uim.create_default_actions()

        gaction_group = self.project.uim.get_action_groups()[0]

        self.project.uim.remove_action_group(gaction_group)

        action_groups = self.project.uim.get_action_groups()

        self.assertEqual(len(action_groups), 0)

    def testCreate(self):
        self.project.uim.reset()
        self.assertEqual(isinstance(self.project.uim._uim, gtk.UIManager), True)

    def testGetUI(self):
        xml = """<toolbar action='toolbar1' name='toolbar1'/>"""
        gadget = FakeGadget('toolbar1')

        self.project.uim.reset()

        self.project.uim.add_ui(gadget, xml)

        xml_string, merge_id = self.project.uim.get_ui(gadget, 'initial-state')

        self.assertEqual(xml_string, xml)
        self.assertEqual(merge_id > 0, True)

        # now, let's get all of them
        uis = self.project.uim.get_ui(gadget)
        self.assertEqual(isinstance(uis, dict), True)
        self.assertEqual(len(uis.keys()), 1)
        self.assertEqual(uis.keys(), ['initial-state'])
        self.assertEqual(uis['initial-state'], (xml_string, merge_id))

    def testGetGtkGadget(self):
        xml = """<toolbar action='toolbar1' name='toolbar1'/>"""
        gadget = FakeGadget('toolbar1')

        self.project.uim.reset()

        # we haven't added the ui yet
        self.assertEqual(self.project.uim.get_widget(gadget), None)


        self.project.uim.add_ui(gadget, xml)

        widget = self.project.uim.get_widget(gadget)
        self.assertEqual(isinstance(widget, gtk.Toolbar), True)

    def testUpdateUI(self):
        xml1 = """<toolbar action='toolbar1' name='toolbar1'/>"""
        gadget = self.create_toolbar()
        old_widget = gadget.widget
        old_parent = old_widget.get_parent()

        wc = WidgetsCounter(len(self.project.get_widgets()))
        old_count = wc.n

        def gadget_added(project, gadget, wc):
            wc.n += 1

        def gadget_removed(project, gadget, wc):
            wc.n -= 1

        con1 = self.project.connect('add-gadget', gadget_added, wc)
        con2 = self.project.connect('remove-gadget', gadget_removed, wc)

        self.project.uim.update_ui(gadget, xml1)

        new_widget = gadget.widget
        new_parent = new_widget.get_parent()

        self.assertEqual(isinstance(new_widget, gtk.Toolbar), True)
        self.assertEqual(old_parent, new_parent)

        new_count = wc.n
        self.assertEqual(old_count, new_count)

        self.project.disconnect(con1)
        self.project.disconnect(con2)

    def testUpdateGadgetName(self):
        gadget = self.create_toolbar()
        xml_string1 = self.project.uim.get_ui(gadget, 'initial-state')[0]

        gadget.name = 'toolbar2'

        self.project.uim.update_widget_name(gadget)

        xml_string2 = self.project.uim.get_ui(gadget, 'initial-state')[0]

        self.assertEqual(xml_string1.replace('toolbar1', 'toolbar2'),
                         xml_string2)

    def _testSave(self):
        # Disabled, I refactored the saving to be part of filewriter -- Johan
        self.project.uim.create_default_actions()
        gadget1 = FakeGadget('toolbar1')
        xml1 = """<toolbar action="toolbar1" name="toolbar1"/>"""
        gadget2 = FakeGadget('menubar1')
        xml2 = """<menubar action="menubar1" name="menubar1"/>"""

        self.project.uim.add_ui(gadget1, xml1)
        self.project.uim.add_ui(gadget2, xml2)

        doc = xml.dom.getDOMImplementation().createDocument(None, None, None)

        node = self.project.uim.save(doc, [gadget1, gadget2], 'libglade')
        pathname = "%s.glade" % os.path.splitext(__file__)[0]

        # let's compare the good file with the one we just create now

        new_pathname = pathname + '.tmp'
        xw = XMLWriter()
        xw.write_node(new_pathname, node)

        diff_files(pathname, new_pathname)

        os.unlink(new_pathname)

    def testLoad(self):
        pathname = os.path.join(os.path.dirname(__file__),
                                "uimanager-example.glade")
        wt = GazpachoObjectBuilder(filename=pathname,
                                   placeholder=Placeholder)
        # load the uim
        self.project.uim.load(wt)

        # we should have again 10 actions and 1 action group
        action_groups = self.project.uim._uim.get_action_groups()
        self.assertEqual(len(action_groups), 1)

        ag = action_groups[0]

        self.assertEqual(ag.get_name(), 'DefaultActions')

        actions = ag.list_actions()

        self.assertEqual(len(actions), 10)

        menubar1 = self.project.uim._loaded_uis['menubar1']
        toolbar1 = self.project.uim._loaded_uis['toolbar1']
        self.assertEqual(isinstance(menubar1, list), True)
        self.assertEqual(isinstance(toolbar1, list), True)
        self.assertEqual(len(menubar1), 1)
        self.assertEqual(len(toolbar1), 1)
        self.assertEqual(menubar1[0][0], 'initial-state')
        self.assertEqual(toolbar1[0][0], 'initial-state')
        self.assertEqual(isinstance(menubar1[0][1], basestring), True)
        self.assertEqual(isinstance(toolbar1[0][1], basestring), True)

        # there is no easy way to test load_widget because the gadget
        # parameter to this function has to be a valid GazpachoGadget with
        # a real Toolbar/Menubar widget properly placed in the widget
        # hierarchy. So we just simulate the project.open behavior

        widgets = []
        for widget in wt._widgets.values():
            if isinstance(widget, gtk.Widget):
                if widget.flags() & gtk.TOPLEVEL:
                    self.assertEqual(widget.get_property('visible'), False)
                    load_gadget_from_widget(widget, self.project)
                    widgets.append(widget)

        window = widgets[0]
        vbox = window.get_child()
        menubar = vbox.get_children()[0]
        self.assertEqual(isinstance(menubar, gtk.MenuBar), True)
        gadget = Gadget.from_widget(menubar)
        # Disabled for now, I broke it when fixing #315625 - Johan
        #self.assertEqual(menubar, self.project.uim.get_widget(gadget))

    def testGroupUIDefinitions(self):
        self.project.uim.reset()

        gadgets = []
        xml_strings = []
        for i in range(10):
            gadgets.append(FakeGadget('toolbar%d' % i))
            xml = "<toolbar action='toolbar%d' name='toolbar%d'/>" % (i, i)
            xml_strings.append(xml)
            self.project.uim.add_ui(gadgets[i], xml)

        uis = self.project.uim._group_ui_definitions_by_name(gadgets[2:7])

        for name, strings in uis.items():
            strings.sort()
            uis[name] = strings

        expected_result = {'initial-state' : [x for x in xml_strings[2:7]] }

        self.assertEqual(uis, expected_result)

    def testCutPaste(self):
        # Quick and dirty test for #337273
        bar = self.create_toolbar()
        widget_name = bar.name

        for i in range(10): # change it to 1000 if you are really paranoid
            # do a cut
            clipboard.cut(bar)

            # then a paste
            placeholder = self.window.widget.get_child()
            clipboard.paste(placeholder, self.project)

            # get ready for next cut
            bar = self.project.get_gadget_by_name(widget_name)


    def testToolbarDeletion(self):

        bar = self.create_toolbar()
        name = bar.name

        cmd = CommandCreateDelete(self.project, bar, parent=bar.get_parent(), create=False)
        cmd.execute()

        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)

        cmd.undo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, gtk.Toolbar), "%s is not a Toolbar" % tmp
        self.assertEqual(name, tmp.get_name())
        tmp_gadget = Gadget.from_widget(tmp)
        assert tmp_gadget is bar
        assert self.project.uim.get_ui(tmp_gadget)
        assert self.project.uim._uim.get_widget('/%s' % name)

        cmd.redo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)

    def testToolbarDeletionRecursive(self):

        bar = self.create_toolbar()
        name = bar.name

        cmd = CommandCreateDelete(self.project, self.window, create=False)
        cmd.execute()

        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)

        cmd.undo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, gtk.Toolbar), "%s is not a Toolbar" % tmp
        self.assertEqual(name, tmp.get_name())
        tmp_gadget = Gadget.from_widget(tmp)
        assert tmp_gadget is bar
        assert self.project.uim.get_ui(tmp_gadget)
        assert self.project.uim._uim.get_widget('/%s' % name)

        cmd.redo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)

    def testMenuBarDeletion(self):

        bar = self.create_menubar()
        name = bar.name

        cmd = CommandCreateDelete(self.project, bar, parent=bar.get_parent(), create=False)
        cmd.execute()

        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)

        cmd.undo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, gtk.MenuBar), "%s is not a MenuBar" % tmp
        self.assertEqual(name, tmp.get_name())
        tmp_gadget = Gadget.from_widget(tmp)
        assert tmp_gadget is bar
        assert self.project.uim.get_ui(tmp_gadget)
        assert self.project.uim._uim.get_widget('/%s' % name)

        cmd.redo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)

    def testMenuBarDeletionRecursive(self):

        bar = self.create_menubar()
        name = bar.name

        cmd = CommandCreateDelete(self.project, self.window, create=False)
        cmd.execute()

        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)

        cmd.undo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, gtk.MenuBar), "%s is not a MenuBar" % tmp
        self.assertEqual(name, tmp.get_name())
        tmp_gadget = Gadget.from_widget(tmp)
        assert tmp_gadget is bar
        assert self.project.uim.get_ui(tmp_gadget)
        assert self.project.uim._uim.get_widget('/%s' % name)

        cmd.redo()
        tmp = self.window.widget.get_child()
        assert isinstance(tmp, Placeholder), "%s is not Placeholder" % tmp
        assert not self.project.uim.get_ui(bar)
        assert not self.project.uim._uim.get_widget('/%s' % name)
