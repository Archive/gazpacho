import common
import gtk

from gazpacho import actioneditor
from gazpacho.gaction import GActionGroup
from gazpacho.commandmanager import command_manager

class DummyGActionGroupDialog:

    def __init__(self, toplevel=None, action_group=None):
        pass

    def run(self):
        return gtk.RESPONSE_OK

    def get_action_group_name(self):
        return "Dummy name"

    def destroy(self):
        pass

class DummyGActionDialog:

    def __init__(self, toplevel, action_group, action=None):
        pass

    def run(self):
        return gtk.RESPONSE_OK

    def get_values(self):
        values = {'name': "test_name",
                  'label': "test_label",
                  'accelerator': "",
                  'tooltip': "",
                  'callback': "",
                  'stock_id': None,
                  'short_label': '',
                  'is_important': False}
        return values

    def destroy(self):
        pass

class SizeGroupViewTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        self.action_editor = self.app.gactions_view

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testAddActionGroup(self):
        # We need to replace the dialog since it is blocking
        actioneditor.GActionGroupDialog = DummyGActionGroupDialog

        # This just tests that the code executes
        self.action_editor.add_action_group()

        # XXX more tests? E.g. that we actually added the group

    def testAddAction(self):
        # We need to replace the dialog since it is blocking
        actioneditor.GActionDialog = DummyGActionDialog

        cmd_class = actioneditor.CommandAddRemoveActionGroup
        cmd = cmd_class(GActionGroup("test_name"), self.project, True)
        gaction_group = command_manager.execute(cmd, self.project)

        # This just tests that the code executes
        self.action_editor.add_action(gaction_group)
