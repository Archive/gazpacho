import os

from kiwi.component import provide_utility
from twisted.trial import unittest

def no_custom_ui():
    return

# Monkey patching catalog.py to avoid custom uis during tests
from gazpacho import catalog
catalog.original_get_custom_ui_filename = catalog.get_custom_ui_filename
catalog.get_custom_ui_filename = no_custom_ui # kids, don't do this at home

from gazpacho.app.app import Application
from gazpacho.interfaces import IGazpachoApp, IPluginManager
from gazpacho.placeholder import Placeholder
from gazpacho.plugins import PluginManager
from gazpacho.project import Project
from gazpacho.gadget import Gadget
from gazpacho.widgetregistry import widget_registry

plugin_manager = PluginManager()
provide_utility(IPluginManager, plugin_manager)

app = Application()
provide_utility(IGazpachoApp, app)

def error(msg):
    print msg

# Make sure we don't block on errors
# XXX this should probably be fixed in a more general manner
import gazpacho.app.app
gazpacho.app.app.error = error

class GazpachoTest(unittest.TestCase):

    def setUp(self):
        self.app = app
        app.new_project()
        self.project = app.get_current_project()

    def tearDown(self):
        self.app.close_current_project()

    def open_filename(self, filename):
        project = Project(self.app)
        project.load(filename)
        return project

    def open_buffer(self, buffer):
        project = Project(self.app)
        project.load_from_buffer(buffer)
        return project

    def create_gadget(self, class_name):
        """Convenient method for creating widgets"""
        adaptor = widget_registry.get_by_name(class_name)
        gadget = Gadget(adaptor, self.project)
        gadget.create_widget(interactive=False)
        return gadget

    def add_child(self, gparent, child_class_name, placeholder):
        """Create a widget of class 'child_class_name' and add it to the
        parent in the placeholder specified.
        """
        gchild = self.create_gadget(child_class_name)
        Gadget.replace(placeholder, gchild.widget, gparent)
        return gchild

    def remove_child(self, gparent, gchild):
        """Remove gchild from gparent and put a placeholder in that place"""
        ph = Placeholder()
        Gadget.replace(gchild.widget, ph, gparent)
        #gparent.set_default_gtk_packing_properties(ph, gchild.adaptor)

    def remove_file(self, filename):
        if os.path.exists(filename):
            os.remove(filename)
