import common
import gtk


from gazpacho.sizegroup import GSizeGroup, safe_to_add_gadgets
from gazpacho import sizegroupeditor
from gazpacho.sizegroupeditor import SizeGroupDialog, \
     add_sizegroup_gadgets, CommandAddRemoveSizeGroupGadgets
from gazpacho.commandmanager import command_manager
from gazpacho.util import select_iter

class SizeGroupTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(3)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        ph1, ph2, ph3 = children

        # add label1
        self.label1 = self.add_child(self.vbox, 'GtkLabel', ph1)

        # add label2
        self.label2 = self.add_child(self.vbox, 'GtkLabel', ph2)

        # add label3
        self.label3 = self.add_child(self.vbox, 'GtkLabel', ph3)

    def testInit(self):
        name = "test"
        mode = gtk.SIZE_GROUP_VERTICAL
        sizegroup = GSizeGroup(name, gtk.SizeGroup(mode))

        assert sizegroup.name == name, "Sizegroup name '%s' != '%s'" % (sizegroup.name, name)
        assert sizegroup.mode == mode, "Sizegroup mode '%s' != '%s'" % (sizegroup.mode, mode)
        assert sizegroup.is_empty(), "Sizegroup should be empty"

    def testInitFail(self):
        try:
            sizegroup = GSizeGroup('test', 'not a gtk.SizeGroup')
        except TypeError:
            pass
        else:
            self.fail("Expected a TypeError")

    def testIsEmpty(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        assert sizegroup.is_empty(), "Sizegroup should be empty"

        sizegroup.add_gadgets([self.label1, self.label2])
        assert not sizegroup.is_empty(), "Sizegroup should not be empty"

    def testGetGadgets(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))

        new_gadgets = [self.label1, self.label2, self.label3]
        sizegroup.add_gadgets(new_gadgets)

        gadgets = sizegroup.get_gadgets()
        assert gadgets and len(gadgets) == len(new_gadgets)
        for gadget in gadgets:
            assert gadget in new_gadgets

    def testHasGadget(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))

        new_gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(new_gadgets)

        assert sizegroup.has_gadget(self.label1), "Should have found the gadget"
        assert not sizegroup.has_gadget(self.label3), "Should not have found the gadgets"

    def testAddGadgets(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        assert sizegroup.is_empty(), "Sizegroup should be empty"

        sizegroup.add_gadgets([self.label1])
        assert len(sizegroup.get_gadgets()) == 1, "Should have 1 gadget"
        assert self.label1 in sizegroup.get_gadgets()

        sizegroup.add_gadgets([self.label2, self.label3])
        assert len(sizegroup.get_gadgets()) == 3, "Should have 3 gadgets"
        assert self.label1 in sizegroup.get_gadgets()
        assert self.label2 in sizegroup.get_gadgets()
        assert self.label3 in sizegroup.get_gadgets()

    def testAddGadgetsEmpty(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        assert sizegroup.is_empty(), "Sizegroup should be empty"

        sizegroup.add_gadgets([self.label1])
        assert len(sizegroup.get_gadgets()) == 1, "Should have 1 gadget"
        assert self.label1 in sizegroup.get_gadgets()

        sizegroup.add_gadgets([])
        assert len(sizegroup.get_gadgets()) == 1, "Should have 1 gadget"
        assert self.label1 in sizegroup.get_gadgets()

    def testRemoveGadgets(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        assert sizegroup.is_empty(), "Sizegroup should be empty"

        sizegroup.add_gadgets([self.label1, self.label2, self.label3])
        assert len(sizegroup.get_gadgets()) == 3, "Should have one gadget"

        sizegroup.remove_gadgets([self.label1])
        assert len(sizegroup.get_gadgets()) == 2, "Should have 2 gadgets"
        assert self.label1 not in sizegroup.get_gadgets()
        assert self.label2 in sizegroup.get_gadgets()
        assert self.label3 in sizegroup.get_gadgets()

        sizegroup.remove_gadgets([self.label2, self.label3])
        assert len(sizegroup.get_gadgets()) == 0, "Should have 0 gadgets"

    def testRemoveGadgetsEmpty(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        assert sizegroup.is_empty(), "Sizegroup should be empty"

        sizegroup.add_gadgets([self.label1, self.label2, self.label3])
        assert len(sizegroup.get_gadgets()) == 3, "Should have one gadget"

        sizegroup.remove_gadgets([])
        assert len(sizegroup.get_gadgets()) == 3, "Should have one gadget"

    def testMode(self):
        mode = gtk.SIZE_GROUP_VERTICAL
        sizegroup = GSizeGroup("test", gtk.SizeGroup(mode))
        assert sizegroup.mode == mode, "Sizegroup mode '%s' != '%s'" % (sizegroup.mode, mode)

        new_mode = gtk.SIZE_GROUP_BOTH
        sizegroup.mode = new_mode
        assert sizegroup.mode == new_mode, "Sizegroup mode '%s' != '%s'" % (sizegroup.mode, new_mode)

    def testName(self):
        name = "test"
        sizegroup = GSizeGroup(name, gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        assert sizegroup.name == name, "Sizegroup name '%s' != '%s'" % (sizegroup.name, name)

        new_name = name + "_new"
        sizegroup.name = new_name
        assert sizegroup.name == new_name, "Sizegroup name '%s' != '%s'" % (sizegroup.name, new_name)


    def testCanAddGadget(self):

        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        sizegroup.add_gadgets([self.vbox])

        # Gadget is in sizegroup -> Ok ??
        result = safe_to_add_gadgets(sizegroup, [self.vbox])
        assert result

        # Gadget's parent is in sizegroup -> Fail
        result = safe_to_add_gadgets(sizegroup, [self.label1])
        assert not result

        # Gadget has a child in the sizegroup
        result = safe_to_add_gadgets(sizegroup, [self.window])
        assert not result


    def testSafeToAddGadgets(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))

        # Test add siblings
        result = safe_to_add_gadgets(sizegroup, [self.label1, self.label2])
        assert result

        # Test add parent and child
        result = safe_to_add_gadgets(sizegroup, [self.label1, self.vbox])
        assert not result

        sizegroup.add_gadgets([self.vbox])

        # Test add parent to sizegroup widget
        result = safe_to_add_gadgets(sizegroup, [self.window])
        assert not result

        # Test add child to sizegroup widget
        result = safe_to_add_gadgets(sizegroup, [self.label1])
        assert not result


class SizeGroupCommandTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(3)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        ph1, ph2, ph3 = children

        # add label1
        self.label1 = self.add_child(self.vbox, 'GtkLabel', ph1)

        # add label2
        self.label2 = self.add_child(self.vbox, 'GtkLabel', ph2)

        # add label3
        self.label3 = self.add_child(self.vbox, 'GtkLabel', ph3)


    def testInitCommand(self):
        gadgets = [self.label1, self.label2]
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, gadgets, self.project,
                                               True)
        assert cmd._sizegroup == sizegroup, "Sizegroup not set"
        for gadget in gadgets:
            assert gadget in cmd._gadgets, "Gadget not found in command"

    def testAddAll(self):
        gadgets = [self.label1, self.label2]
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        add = True
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, gadgets, self.project,
                                               add)

        assert not self.project.sizegroups, "The project shouldn't have any sizegroups"

        # Execute add
        cmd.execute()
        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have added 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        assert sizegroup.get_gadgets() and len(sizegroup.get_gadgets()) == 2, "Should have added two widetgs"
        for gadget in gadgets:
            assert gadget in sizegroup.get_gadgets(), "Gadget not in sizegroup"

        # Undo add
        cmd.execute()
        assert not self.project.sizegroups, "Should have removed the sizegroup"
        assert sizegroup not in self.project.sizegroups, "Sizegroup should not be in project"
        assert not sizegroup.get_gadgets(), "Sizegroup shouldn't have any gadgets"

    def testAdd(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        default_gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(default_gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # Create command
        gadgets = [self.label3]
        add = True
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, gadgets, self.project,
                                               add)
        # Execute add
        cmd.execute()
        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        assert sizegroup.get_gadgets() and len(sizegroup.get_gadgets()) == 3, "Should have 3 widetgs in sizegroup"
        for gadget in default_gadgets:
            assert gadget in sizegroup.get_gadgets(), "Default gadget not in sizegroup"
        for gadget in gadgets:
            assert gadget in sizegroup.get_gadgets(), "New gadget not in sizegroup"

        # Undo add
        cmd.execute()
        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        for gadget in default_gadgets:
            assert gadget in sizegroup.get_gadgets(), "Default gadget not in sizegroup"
        for gadget in gadgets:
            assert gadget not in sizegroup.get_gadgets(), "New gadget still in sizegroup"

    def testRemove(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        default_gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(default_gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # Create command
        gadgets = [self.label2]
        add = False
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, gadgets, self.project,
                                               add)
        # Execute remove
        cmd.execute()
        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        assert sizegroup.get_gadgets() and len(sizegroup.get_gadgets()) == 1, "Should have 1 widetg in sizegroup"
        assert self.label1 in sizegroup.get_gadgets(), "Should have found labels1 in sizegroup"
        assert self.label2 not in sizegroup.get_gadgets(), "Should not have found labels2 in sizegroup"

        # Undo add
        cmd.execute()
        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"
        for gadget in default_gadgets:
            assert gadget in sizegroup.get_gadgets(), "Should have found gadget in sizegroup"

    def testRemoveAll(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # Create command
        add = False
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, gadgets, self.project, add)
        # Execute remove
        cmd.execute()
        assert not self.project.sizegroups, "Project shouldn't have any sizegroups"
        assert not sizegroup.get_gadgets(), "Sizegroup should not have any gadgets"

        # Undo add
        cmd.execute()
        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"
        for gadget in gadgets:
            assert gadget in sizegroup.get_gadgets(), "Should have found gadget in sizegroup"

class GadgetDeletedTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(3)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        ph1, ph2, ph3 = children

        # add label1
        self.label1 = self.add_child(self.vbox, 'GtkLabel', ph1)

        # add label2
        self.label2 = self.add_child(self.vbox, 'GtkLabel', ph2)

    def testInit(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

    def testDeletedGadget(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"
        assert sizegroup.has_gadget(self.label1), "Gadget should be in sizegroup"
        assert sizegroup.has_gadget(self.label2), "Gadget should be in sizegroup"

        # mark the gadget as deleted
        self.label1.deleted = True
        assert not sizegroup.has_gadget(self.label1), "Gadget should have been removed from sizegroup"
        assert sizegroup.has_gadget(self.label2), "Gadget should still be in sizegroup"

        # mark the gadget as not deleted
        self.label1.deleted = False
        assert sizegroup.has_gadget(self.label1), "Gadget should still be in sizegroup"
        assert sizegroup.has_gadget(self.label2), "Gadget should still be in sizegroup"


    def testDeletedToplevelGadget(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup.has_gadget(self.label1), "Gadget should be in sizegroup"
        assert sizegroup.has_gadget(self.label2), "Gadget should be in sizegroup"

        # mark the toplevel as deleted
        self.window.deleted = True
        assert not sizegroup.has_gadget(self.label1), "Gadget should have been removed from sizegroup"
        assert not sizegroup.has_gadget(self.label2), "Gadget should have been removed from sizegroup"


        # mark the toplevel as not deleted
        self.window.deleted = False
        assert sizegroup.has_gadget(self.label1), "Gadget should still be in sizegroup"
        assert sizegroup.has_gadget(self.label2), "Gadget should still be in sizegroup"


    def testDeletedToplevelGadget2(self):
        # Test to see that the sizegroup automatically is removed when
        # all widgets are removed. Doesn't work due to the introducion
        # of the reference container but it's a minor issue.
        
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(gadgets)
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # mark the toplevel as deleted
        self.window.deleted = True
        assert sizegroup not in self.project.sizegroups, "Sizegroup should not be in project"

        # mark the toplevel as not deleted
        self.window.deleted = False
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"
    testDeletedToplevelGadget2.skip = "Doesn't work since the introduction of the reference container but it's a minor issue."

class CommandManagerTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(3)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        ph1, ph2, ph3 = children

        # add label1
        self.label1 = self.add_child(self.vbox, 'GtkLabel', ph1)

        # add label2
        self.label2 = self.add_child(self.vbox, 'GtkLabel', ph2)

        # add label3
        self.label3 = self.add_child(self.vbox, 'GtkLabel', ph3)

    def testAddGadgets(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        default_gadgets = [self.label1]
        sizegroup.add_gadgets(default_gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # Add gadgets
        gadget = self.label2
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, [gadget],
                                               self.project, True)
        command_manager.execute(cmd, self.project)

        assert gadget in sizegroup.get_gadgets(), "Gadget should be in sizegroup"

        # Undo add
        command_manager.undo(self.project)

        assert gadget not in sizegroup.get_gadgets(), "Gadget should not be in sizegroup"


    def testAddExistingadgets(self):

        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        default_gadgets = [self.label1, self.label2]
        sizegroup.add_gadgets(default_gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # Add gadgets
        gadget = self.label2
        add_sizegroup_gadgets(self.project, sizegroup, [gadget])

        assert len(sizegroup.get_gadgets()) == 2, "%s != 2" % len(sizegroup.get_gadgets())
        assert gadget in sizegroup.get_gadgets(), "Gadget should be in sizegroup"

        # Undo add
        command_manager.undo(self.project)

        assert len(sizegroup.get_gadgets()) == 2, "Should have found 2 gadgets"
        assert self.label2 in sizegroup.get_gadgets(), "Gadget should not have been removed"

    def testRemoveGadgets(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        self.project.sizegroups.append(sizegroup)

        default_gadgets = [self.label1, self.label2, self.label3]
        sizegroup.add_gadgets(default_gadgets)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # Remove gadgets
        gadgets = [self.label2, self.label3]
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, gadgets,
                                               self.project, False)
        command_manager.execute(cmd, self.project)

        for gadget in gadgets:
            assert gadget not in sizegroup.get_gadgets(), "Gadget should not be in sizegroup"

        # Undo remove
        command_manager.undo(self.project)

        for gadget in gadgets:
            assert gadget in sizegroup.get_gadgets(), "Gadget should be in sizegroup"

    def testRemoveSizegroup(self):
        # Init data
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))

        gadgets = [self.label1, self.label2, self.label3]
        sizegroup.add_gadgets(gadgets)

        self.project.sizegroups.append(sizegroup)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        # Remove sizegroup
        gadgets = sizegroup.get_gadgets()
        cmd = CommandAddRemoveSizeGroupGadgets(sizegroup, gadgets,
                                               self.project, False)
        command_manager.execute(cmd, self.project)

        assert not self.project.sizegroups, "Project should not have any sizegroups"
        assert not sizegroup.get_gadgets(), "Sizegroup should not have any gadgets"
        for gadget in gadgets:
            prop = gadget.get_prop('sizegroup')
            assert prop.value is None, "sizegroup property should have been cleared"

        # Undo remove
        command_manager.undo(self.project)

        assert self.project.sizegroups and len(self.project.sizegroups) == 1, "Should have 1 sizegroup"
        assert sizegroup in self.project.sizegroups, "Sizegroup not in project"

        for gadget in gadgets:
            assert gadget in sizegroup.get_gadgets(), "Gadget should be in sizegroup"
            prop = gadget.get_prop('sizegroup')
            assert prop.value == sizegroup.name, "The sizegroup property should be set to the sizegroup name"

class DummySizegroupDialog:
    """
    This is just a dummy dialog that is used by the SizeGroupViewTest.
    """

    response = gtk.RESPONSE_OK

    def __init__(self, parent, sizegroups):
        pass

    def run(self):
        return self.response

    def destroy(self):
        pass

    def get_selected_sizegroup(self):
        sizegroup = GSizeGroup("test_name", gtk.SizeGroup(gtk.SIZE_GROUP_HORIZONTAL))
        return sizegroup

class SizeGroupViewTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        self.sizegroup_view = self.app.sizegroup_view

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)


        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(3)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        ph1, ph2, ph3 = children

        # add label1
        self.label1 = self.add_child(self.vbox, 'GtkLabel', ph1)

        # add label2
        self.label2 = self.add_child(self.vbox, 'GtkLabel', ph2)

    def testFindSizeGroup(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)

        # Test a failed find
        sizegroup_iter = self.sizegroup_view._find_sizegroup(sizegroup)
        assert not sizegroup_iter, "Should not have found the sizegroup"

        # Add the sizegroup and test for success
        self.project.add_sizegroup(sizegroup)

        sizegroup_iter = self.sizegroup_view._find_sizegroup(sizegroup)
        assert sizegroup_iter, "Should have found the sizegroup"

    def testFindGadget(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)

        # Test with wrong sizegroup
        gadget_iter = self.sizegroup_view._find_gadget(sizegroup, self.label1)
        assert not gadget_iter, "Should not have found the gadget"

        # Add the sizegroup and test for success
        self.project.add_sizegroup(sizegroup)

        # Test with wrong gadget
        gadget_iter = self.sizegroup_view._find_gadget(sizegroup, self.label2)
        assert not gadget_iter, "Should not have found the gadget"

        # Test with correct gadget and sizegroup
        gadget_iter = self.sizegroup_view._find_gadget(sizegroup, self.label1)
        assert gadget_iter, "Should have found the gadget"


    def testSetProjectNone(self):
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)
        self.project.add_sizegroup(sizegroup)

        sizegroup_iter = self.sizegroup_view._find_sizegroup(sizegroup)
        assert sizegroup_iter, "Should have found the sizegroup"

        # Set project to None
        self.sizegroup_view.set_project(None)

        sizegroup_iter = self.sizegroup_view._find_sizegroup(sizegroup)
        assert not sizegroup_iter, "Should not have found the sizegroup"

    def testSetProject(self):
        old_project = self.project
        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)
        old_project.add_sizegroup(sizegroup)

        sizegroup_iter = self.sizegroup_view._find_sizegroup(sizegroup)
        assert sizegroup_iter, "Should have found the sizegroup"

        # Set project to None
        self.sizegroup_view.set_project(None)

        sizegroup_iter = self.sizegroup_view._find_sizegroup(sizegroup)
        assert not sizegroup_iter, "Should not have found the sizegroup"

        # Set the previous project again
        self.sizegroup_view.set_project(old_project)

        sizegroup_iter = self.sizegroup_view._find_sizegroup(sizegroup)
        assert sizegroup_iter, "Should have found the sizegroup"


    def testRemoveSelectedItemEmpty(self):
        # Just make user it doesn't crash on an empty model
        self.sizegroup_view.remove_selected_item()

    def testRemoveSelectedSizeGroup(self):
        view = self.sizegroup_view

        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)
        self.project.add_sizegroup(sizegroup)

        # Select the sizegroup
        sizegroup_iter = view._find_sizegroup(sizegroup)
        assert sizegroup_iter, "Should have found a sizegroup iter"
        select_iter(view._treeview, sizegroup_iter)

        # Remove the sizegroup
        view.remove_selected_item()

        sizegroup_iter = view._find_sizegroup(sizegroup)
        assert not sizegroup_iter, "Should not have found a sizegroup iter"

    def testRemoveSelectedGadget(self):
        view = self.sizegroup_view

        sizegroup = GSizeGroup("test", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)
        self.project.add_sizegroup(sizegroup)

        # Select the sizegroup
        gadget_iter = view._find_gadget(sizegroup, self.label1)
        assert gadget_iter, "Should have found a gadget iter"
        select_iter(view._treeview, gadget_iter)

        # Remove the sizegroup
        view.remove_selected_item()

        gadget_iter = view._find_gadget(sizegroup, self.label1)
        assert not gadget_iter, "Should not have found a gadget iter"

    def testOnSizeGroupNameChanged(self):
        # This doens't test anything except that the code is valid
        sizegroup = GSizeGroup("name", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)
        self.project.add_sizegroup(sizegroup)

        self.sizegroup_view._on_sizegroup_name_changed(sizegroup)

    def testOnGadgetNotifyName(self):
        # This doens't test anything except that the code is valid
        sizegroup = GSizeGroup("name", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        gadgets = [self.label1]
        sizegroup.add_gadgets(gadgets)
        self.project.add_sizegroup(sizegroup)
        self.label1.widget.notify('name')

    def testAddSizegroupGadgets(self):
        # Use a dummy dialog since the real one is blocking
        sizegroupeditor.SizeGroupDialog = DummySizegroupDialog

        self.project.selection.clear()
        self.project.selection.add(self.label1.widget)
        self.project.selection.add(self.label2.widget)

        self.assertEqual(len(self.project.sizegroups), 0)
        add_sizegroup_gadgets(self.project)
        self.assertEqual(len(self.project.sizegroups), 1)

    def testAddSizegroupGadgetsCancel(self):
        # Use a dummy dialog since the real one is blocking
        sizegroupeditor.SizeGroupDialog = DummySizegroupDialog
        DummySizegroupDialog.response = gtk.RESPONSE_CANCEL

        assert not self.project.sizegroups, "There should be no sizegroups in the project yet"

        add_sizegroup_gadgets(self.project)
        assert not self.project.sizegroups, "There should still be no sizegroups in the project"


class SizeGroupDialogTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

    def testInit(self):
        sizegroups = [GSizeGroup("test1", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL)),
                      GSizeGroup("test2", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))]
        dialog = SizeGroupDialog(self.app.get_window(), sizegroups)

    def testInitEmpty(self):
        sizegroups = []
        dialog = SizeGroupDialog(self.app.get_window(), sizegroups)

    def testSuggestSizeGroupName(self):
        sizegroups = [GSizeGroup("sizegroup1", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL)),
                      GSizeGroup("sizegroup3", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))]
        dialog = SizeGroupDialog(self.app.get_window(), sizegroups)

        name = dialog._suggest_sizegroup_name()
        assert name == "sizegroup2"

    def testGetSelectedExisting(self):
        sizegroup = GSizeGroup("test1", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))
        dialog = SizeGroupDialog(self.app.get_window(), [sizegroup])

        # This should select the sizegroup by default
        selected_group = dialog.get_selected_sizegroup()
        assert selected_group == sizegroup

    def testGetSelectedNew(self):
        sizegroups = [GSizeGroup("test1", gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))]
        dialog = SizeGroupDialog(self.app.get_window(), sizegroups)
        name = dialog._suggest_sizegroup_name()

        # This should select a new sizegroup
        dialog._new_radio.set_active(True)

        selected_group = dialog.get_selected_sizegroup()
        assert selected_group not in sizegroups
        assert selected_group.name == name

    def testGetSelectedEmpty(self):
        dialog = SizeGroupDialog(self.app.get_window(), [])
        name = dialog._suggest_sizegroup_name()

        # This should create a new sizegroup by default
        selected_group = dialog.get_selected_sizegroup()
        assert selected_group.name == name

    def testGetSizeGroupDict(self):
        dialog = SizeGroupDialog(self.app.get_window(), [])

        # Test empty
        data = dialog._get_sizegroup_dict([])
        assert data == {}, "Should have gotten an empty dict"

        # Test with data
        name1 = "test1"
        name2 = "test2"
        sizegroups = [GSizeGroup(name1, gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL)),
                      GSizeGroup(name2, gtk.SizeGroup(gtk.SIZE_GROUP_VERTICAL))]

        data = dialog._get_sizegroup_dict(sizegroups)
        assert data and len(data) == 2, "Should have found sizegroups of length 2"
        assert data[name1].name == name1
        assert data[name2].name == name2
