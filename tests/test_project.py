from common import GazpachoTest

class ProjectTest(GazpachoTest):
    def testDomain(self):
        self.assertEqual(self.project.get_domain(), '')
        self.project.set_domain('domain')
        self.assertEqual(self.project.get_domain(), 'domain')

    def testDomainFromLoader(self):
        project = self.open_buffer('<glade-interface domain="domain"/>')
        self.assertEqual(project.get_domain(), 'domain')
