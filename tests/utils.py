import difflib
import os.path
import sys

import gtk

currentdir = os.path.dirname(os.path.abspath(sys.argv[0]))
prefix = os.path.abspath(os.path.join(currentdir, '..'))
sys.path.append(prefix)

def refresh_gui():
    while gtk.events_pending():
        gtk.main_iteration_do(block=False)

def _diff(orig, new, short, verbose):
    lines = difflib.unified_diff(orig, new)
    if not lines:
        return

    diff = False
    try:
        first = lines.next()
        diff = True
    except StopIteration:
        pass
    else:
        print
        print '%s: %s' % (short, first[:-1])
        for line in lines:
            print '%s: %s' % (short, line[:-1])

    return diff

def diff_files(orig, new, verbose=False):
    """
    Diff two files.

    @return: True i the files differ otherwise False
    @rtype: bool
    """
    return _diff(open(orig).readlines(),
                 open(new).readlines(),
                 short=os.path.basename(orig),
                 verbose=verbose)

def diff_strings(orig, new, verbose=False):
    """
    Diff two strings.

    @return: True i the files differ otherwise False
    @rtype: bool
    """
    def _tolines(s):
        return [s + '\n' for line in s.split('\n')]

    return _diff(_tolines(orig),
                 _tolines(new),
                 short='<input>',
                 verbose=verbose)
