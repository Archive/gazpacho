# Authors: Mikael Hallendal <micke@imendio.com>
#          Lorenzo Gil Sanchez <lgs@sicem.biz>

import gtk

from gazpacho.widgetregistry import widget_registry

import common
from utils import diff_strings

class SavePropertiesTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testSaveProperties(self):
        """When the draw-value property is set to False this is not
        saved in the glade file. See bug #304539"""

        scale = self.add_child(self.window, 'GtkVScale',
                              self.window.widget.get_child())

        scale.name = 'my-scale'

        scale.get_prop('draw-value').set(False)
        self.assertEqual(scale.widget.get_property('draw-value'), False)

        self.project.save(__file__+'.glade')

        new_project = self.open_filename(__file__+'.glade')
        gadget = new_project.get_gadget_by_name('my-scale')
        self.assertEqual(gadget.widget.get_property('draw-value'), False)
        self.remove_file(__file__+'.glade')

    def testSavePositionProperty(self):
        "The position packing property is not being saved. See bug #313087"
        vbox = self.add_child(self.window, 'GtkVBox',
                              self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        for i in range(3):
            label = self.add_child(vbox, 'GtkLabel',
                                   vbox.widget.get_children()[i])
            self.project.add_widget(label.widget)
            # set gtk default
            label.widget.set_alignment(0.5, 0.5)

        for i in range(3):
            child = vbox.widget.get_children()[i]
            self.assertEqual(type(child), gtk.Label)
            pos = vbox.widget.child_get_property(child, 'position')
            self.assertEqual(pos, i)

        xml_string = """<glade-interface>
    <widget class="GtkVBox" id="vbox1">
        <child>
            <widget class="GtkLabel" id="label1">
                <property name="label" context="yes" translatable="yes">label1</property>
            </widget>
        </child>
        <child>
            <widget class="GtkLabel" id="label2">
                <property name="label" context="yes" translatable="yes">label2</property>
            </widget>
            <packing>
                <property name="position">1</property>
            </packing>
        </child>
        <child>
            <widget class="GtkLabel" id="label3">
                <property name="label" context="yes" translatable="yes">label3</property>
            </widget>
            <packing>
                <property name="position">2</property>
            </packing>
        </child>
    </widget>
</glade-interface>
"""

        if diff_strings(xml_string, vbox.to_xml()):
            raise AssertionError

    def testSaveRulerPositionProperty(self):
        """When saving a GtkRuler inside a GtkBox the 'position' property
        is confused with the 'position' packing property"""
        if not widget_registry.has_name('GtkHRuler'):
            # add the deprecated GtkRuler widget to the catalog to run this
            # test
            return
        vbox = self.add_child(self.window, 'GtkVBox',
                              self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        prop = vbox.get_prop('size')
        prop.set(1)

        ruler = self.add_child(vbox, 'GtkHRuler',
                               vbox.widget.get_children()[0])
        self.project.add_widget(ruler.widget)
        prop = ruler.get_prop('position')
        prop.set(2.5)

        xml_string = """<glade-interface>
    <widget class="GtkVBox" id="vbox1">
        <property name="visible">True</property>
        <child>
            <widget class="GtkHRuler" id="hruler1">
                <property name="position">2.5</property>
                <property name="visible">True</property>
            </widget>
        </child>
    </widget>
</glade-interface>
"""

        if diff_strings(xml_string, vbox.to_xml()):
            raise AssertionError
