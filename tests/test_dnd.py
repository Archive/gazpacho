import common
import gtk

from gazpacho.clipboard import CommandCutPaste
from gazpacho.commandmanager import command_manager
from gazpacho.gadget import Gadget
from gazpacho.placeholder import Placeholder
from gazpacho.widgetregistry import widget_registry
from gazpacho.widgets.base.box import DragAppendCommand, CreateAppendCommand, \
      DragExtendCommand, CreateExtendCommand
from gazpacho.dndhandlers import DND_POS_TOP, DND_POS_RIGHT, CommandDragDrop

class DnDCommandManagerTest(common.GazpachoTest):

    # XXX this code can be refactored a bit since most tests do
    # similar things

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(2)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)

    def testExecuteDrop(self):
        ph1, ph2 = self.vbox.widget.get_children()

        # create a label
        label = self.create_gadget('GtkLabel')
        name = label.name

        # execute command
        cmd = CommandCutPaste(label, self.project, ph1, False)
        command_manager.execute(cmd, self.project)

        w1 = self.vbox.widget.get_children()[0]
        assert w1 is label.widget
        self.assertEqual(w1.get_name(), name)

        # undo
        command_manager.undo(self.project)
        ph1, ph2 = self.vbox.widget.get_children()
        assert isinstance(ph1, Placeholder)
        assert isinstance(ph2, Placeholder)

    def testExecuteDragDrop(self):
        ph1, ph2 = self.vbox.widget.get_children()

        # add a label
        label = self.add_child(self.vbox, 'GtkLabel', ph1)
        self.project.add_widget(label.widget)
        name = label.name

        # execute command
        cmd = CommandDragDrop(label, ph2)
        command_manager.execute(cmd, self.project)

        w1, w2 = self.vbox.widget.get_children()
        assert isinstance(w1, Placeholder)
        assert isinstance(w2, gtk.Label)
        self.assertEqual(w2.get_name(), name)

        # undo
        command_manager.undo(self.project)
        w, p = self.vbox.widget.get_children()
        self.assertEqual(w.get_name(), name)
        assert isinstance(p, Placeholder)

    def testExecuteDragAppend(self):
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph1, ph2 = children

        # add label1
        label1 = self.add_child(self.vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        name1 = label1.name

        # add label2
        label2 = self.add_child(self.vbox, 'GtkLabel', ph2)
        self.project.add_widget(label2.widget)
        name2 = label2.name

        # execute command
        cmd = DragAppendCommand(label1, self.vbox, 2, False)
        command_manager.execute(cmd, self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        w1, w2, w3 = children

        assert isinstance(w1, Placeholder)
        self.assertEqual(w2.name, name2)
        assert w2 is label2.widget
        self.assertEqual(w3.name, name1)

        # undo
        command_manager.undo(self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children
        assert w1 is label1.widget
        self.assertEqual(w1.name, name1)
        assert w2 is label2.widget
        self.assertEqual(w2.name, name2)

    def testExecuteDragAppendKeepSource(self):
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph1, ph2 = children

        # add label1
        label1 = self.add_child(self.vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        name1 = label1.name

        # add label2
        label2 = self.add_child(self.vbox, 'GtkLabel', ph2)
        self.project.add_widget(label2.widget)
        name2 = label2.name

        # execute command
        cmd = DragAppendCommand(label1, self.vbox, 2, True)
        command_manager.execute(cmd, self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        w1, w2, w3 = children

        self.assertEqual(w1.name, name1)
        assert w1 is label1.widget
        self.assertEqual(w2.name, name2)
        assert w2 is label2.widget
        assert isinstance(w3, gtk.Label)
        assert w3.name not in [name1, name2]

        # undo
        command_manager.undo(self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children
        assert w1 is label1.widget
        self.assertEqual(w1.name, name1)
        assert w2 is label2.widget
        self.assertEqual(w2.name, name2)

    def testExecuteCreateAppend(self):
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph1, ph2 = children

        # add label1
        label1 = self.add_child(self.vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        name1 = label1.name

        # add label2
        label2 = self.add_child(self.vbox, 'GtkLabel', ph2)
        self.project.add_widget(label2.widget)
        name2 = label2.name

        adaptor = widget_registry.get_by_name('GtkLabel')
        gadget = Gadget(adaptor, self.project)
        gadget.create_widget(interactive=False)
        cmd = CreateAppendCommand(gadget, self.vbox, 2)
        command_manager.execute(cmd, self.project)


        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 3)
        w1, w2, w3 = children

        self.assertEqual(w1.name, name1)
        assert w1 is label1.widget
        self.assertEqual(w2.name, name2)
        assert w2 is label2.widget
        assert isinstance(w3, gtk.Label)
        assert w3.name not in [name1, name2]

        # undo
        command_manager.undo(self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children
        assert w1 is label1.widget
        self.assertEqual(w1.name, name1)
        assert w2 is label2.widget
        self.assertEqual(w2.name, name2)

    def testExecuteDragExtend(self):
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph1, ph2 = children

        # add label1
        label1 = self.add_child(self.vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        name1 = label1.name

        # add label2
        label2 = self.add_child(self.vbox, 'GtkLabel', ph2)
        self.project.add_widget(label2.widget)
        name2 = label2.name

        # execute command
        cmd = DragExtendCommand(label1, label2, DND_POS_TOP, False)
        command_manager.execute(cmd, self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph, box = children
        assert isinstance(ph, Placeholder)
        assert isinstance(box, gtk.VBox)

        children = box.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children

        self.assertEqual(w1.get_name(), name1)
        self.assertEqual(w2.get_name(), name2)

        # undo
        command_manager.undo(self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children
        assert w1 is label1.widget
        self.assertEqual(w1.name, name1)
        assert w2 is label2.widget
        self.assertEqual(w2.name, name2)

    def testExecuteDragExtendKeepSource(self):
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph1, ph2 = children

        # add label1
        label1 = self.add_child(self.vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        name1 = label1.name

        # add label2
        label2 = self.add_child(self.vbox, 'GtkLabel', ph2)
        self.project.add_widget(label2.widget)
        name2 = label2.name

        # execute command
        cmd = DragExtendCommand(label1, label2, DND_POS_TOP, True)
        command_manager.execute(cmd, self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        l1, box = children
        assert l1 is label1.widget
        self.assertEqual(l1.get_name(), name1)
        assert isinstance(box, gtk.VBox)

        children = box.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children

        assert w1.get_name() not in [name1, name2]
        self.assertEqual(w2.get_name(), name2)

        # undo
        command_manager.undo(self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children
        assert w1 is label1.widget
        self.assertEqual(w1.name, name1)
        assert w2 is label2.widget
        self.assertEqual(w2.name, name2)

    def testExecuteCreateExtend(self):
        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        ph1, ph2 = children

        # add label1
        label1 = self.add_child(self.vbox, 'GtkLabel', ph1)
        self.project.add_widget(label1.widget)
        name1 = label1.name

        # add label2
        label2 = self.add_child(self.vbox, 'GtkLabel', ph2)
        self.project.add_widget(label2.widget)
        name2 = label2.name

        # execute command
        adaptor = widget_registry.get_by_name('GtkLabel')
        gadget = Gadget(adaptor, self.project)
        gadget.create_widget(interactive=False)
        cmd = CreateExtendCommand(gadget, label2, DND_POS_RIGHT)
        command_manager.execute(cmd, self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        l1, box = children
        assert l1 is label1.widget
        self.assertEqual(l1.get_name(), name1)
        assert isinstance(box, gtk.HBox)

        children = box.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children

        self.assertEqual(w1.get_name(), name2)
        assert isinstance(w2, gtk.Label)
        assert w2.get_name() not in [name1, name2]

        # undo
        command_manager.undo(self.project)

        children = self.vbox.widget.get_children()
        self.assertEqual(len(children), 2)
        w1, w2 = children
        assert w1 is label1.widget
        self.assertEqual(w1.name, name1)
        assert w2 is label2.widget
        self.assertEqual(w2.name, name2)
