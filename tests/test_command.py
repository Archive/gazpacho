import common

from gazpacho.clipboard import clipboard
from gazpacho.command import Command
from gazpacho.commandmanager import command_manager
from gazpacho.widgets.base.box import CommandBoxDeletePlaceholder
from gazpacho.propertyeditor import CommandSetTranslatableProperty
from gazpacho.signaleditor import SignalInfo, CommandAddRemoveSignal, \
     CommandChangeSignal
from gazpacho.gadget import Gadget

class DummyCommand(Command):
    """A dummy command used for testing."""

    def __init__(self):
        Command.__init__(self, "Dummy")

        self.undone = False
        self.redone = False

    def undo(self):
        self.undone = True

    def redo(self):
        self.redone = True

class CommandTestHelper(common.GazpachoTest):
    def setUp(self):
        common.GazpachoTest.setUp(self)

class CommandManagerTest(CommandTestHelper):

    def testUndoEmpty(self):
        # Test what happens when calling CommandManager.undo() when
        # the undo list is empty.
        try:
            command_manager.undo(self.project)
        except:
            self.fail()

    def testUndo(self):
        # Test that CommandManager.undo() works as it should.
        cmd = DummyCommand()
        self.project.undo_stack.push_undo(cmd)
        command_manager.undo(self.project)
        self.assertEqual(cmd.undone, True)
        assert not self.project.undo_stack.has_undo()

    def testRedoEmpty(self):
        # Test what happens when calling CommandManager.redo() when
        # the redo list is empty"""
        try:
            command_manager.redo(self.project)
        except:
            self.fail()

    def testRedo(self):
        # Test that CommandManager.redo() works as it should.
        cmd = DummyCommand()
        self.project.undo_stack.push_undo(cmd)
        self.project.undo_stack.pop_undo()
        command_manager.redo(self.project)
        self.assertEqual(cmd.redone, True)
        assert not self.project.undo_stack.has_redo()


    def testCopy(self):
        # Test CommandManager.copy()
        gadget = self.create_gadget("GtkLabel")
        self.project.add_widget(gadget.widget)
        clipboard.copy(gadget)

        clipboard.selected = None
        item = clipboard.get_selected_item()
        self.assertEqual(item.name, gadget.name)

    def testCopyInternal(self):
        # Make sure we cannot copy internal children
        gadget = self.create_gadget("GtkDialog")
        self.project.add_widget(gadget.widget)
        child = Gadget.from_widget(gadget.get_children()[0])

        clipboard.copy(child)
        clipboard.selected = None
        item = clipboard.get_selected_item()
        if item:
            self.assertNotEqual(item.name, child.name)

    def testCut(self):
        # Test CommandManager.copy()
        gadget = self.create_gadget("GtkDialog")
        self.project.add_widget(gadget.widget)
        clipboard.cut(gadget)

        clipboard.selected = None
        item = clipboard.get_selected_item()
        self.assertEqual(item.name, gadget.name)

    def testCutInternal(self):
        # Make sure we cannot copy internal children
        gadget = self.create_gadget("GtkDialog")
        self.project.add_widget(gadget.widget)
        child = Gadget.from_widget(gadget.get_children()[0])

        clipboard.cut(child)
        clipboard.selected = None
        item = clipboard.get_selected_item()
        if item:
            self.assertNotEqual(item.name, child.name)

class SignalCommandTest(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        self.window = self.create_gadget('GtkWindow')
        self.signal = SignalInfo(name='active-default',
                                 handler='my_handler')

    def _assert_has_signal(self, signal = None):
        if not signal:
            signal = self.signal

        test_signals = self.window._signals.get(signal.name, [])
        assert len(test_signals) == 1
        assert test_signals[0].handler == signal.handler
        assert test_signals[0].after == signal.after

    def _assert_no_signal(self, signal = None):
        if not signal:
            signal = self.signal

        test_signals = self.window._signals.get(self.signal.name)
        assert not test_signals

    def testAddSignalCommand(self):
        # Test the add signal command
        cmd = CommandAddRemoveSignal(self.window, self.signal, True)
        cmd.execute()
        self._assert_has_signal()

        cmd.undo()
        self._assert_no_signal()

        cmd.redo()
        self._assert_has_signal()

    def testAddSignalManager(self):
        # Test add signal in the command manager
        cmd = CommandAddRemoveSignal(self.window, self.signal, True)
        command_manager.execute(cmd, self.project)
        self._assert_has_signal()

    def testRemoveSignal(self):
        # Test the remove signal command
        self.window.add_signal_handler(self.signal)

        cmd = CommandAddRemoveSignal(self.window, self.signal, False)

        cmd.execute()
        self._assert_no_signal()

        cmd.undo()
        self._assert_has_signal()

        cmd.redo()
        self._assert_no_signal()

    def testRemoveSignalManager(self):
        # Test remove signal in the command manager
        self.window.add_signal_handler(self.signal)
        cmd = CommandAddRemoveSignal(self.window, self.signal, False)
        command_manager.execute(cmd, self.project)
        self._assert_no_signal()

    def testChangeSignal(self):
        # Test the change signal command
        self.window.add_signal_handler(self.signal)

        new_signal = SignalInfo(name=self.signal.name,
                                handler='new_handler',
                                after=True)


        cmd =  CommandChangeSignal(self.window, self.signal, new_signal)

        cmd.execute()
        self._assert_has_signal(new_signal)

        cmd.undo()
        self._assert_has_signal()

        cmd.redo()
        self._assert_has_signal(new_signal)

    def testChangeSignalManager(self):
        # Test change signal in the command manager
        self.window.add_signal_handler(self.signal)
        new_signal = SignalInfo(name=self.signal.name,
                                handler='new_handler',
                                after=True)

        cmd = CommandChangeSignal(self.window, self.signal, new_signal)
        command_manager.execute(cmd, self.project)
        self._assert_has_signal(new_signal)



class TestTranslatableProperty(common.GazpachoTest):

    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

        # add a box
        self.vbox = self.add_child(self.window, 'GtkVBox',
                                   self.window.widget.get_child())
        self.project.add_widget(self.vbox.widget)
        self.vbox.get_prop('size').set(2)

    def testSetTranslatableProperty(self):
        prop = self.window.get_prop('title')
        prop.set('before')
        self.assertEqual(prop.value, 'before')
        command_manager.execute(CommandSetTranslatableProperty(prop, 'value',
                                                               'comment',
                                                               True, False),
                                self.project)
        self.assertEqual(prop.value, 'value')
        command_manager.undo(self.project)
        self.assertEqual(prop.value, 'before')
