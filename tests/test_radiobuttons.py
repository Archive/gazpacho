import common

from gazpacho.commandmanager import command_manager
from gazpacho.propertyeditor import CommandSetProperty

class GtkRadioButtonsTest(common.GazpachoTest):
    def setUp(self):
        common.GazpachoTest.setUp(self)

        # add a window
        self.window = self.create_gadget('GtkWindow')
        self.window.get_prop('visible').set(False)
        self.project.add_widget(self.window.widget)

    def testSetGroup(self):
        vbox = self.add_child(self.window, 'GtkVBox',
                               self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        rb1 = self.add_child(vbox, 'GtkRadioButton',
                             vbox.widget.get_children()[0])
        self.project.add_widget(rb1.widget)
        rb2 = self.add_child(vbox, 'GtkRadioButton',
                             vbox.widget.get_children()[1])
        self.project.add_widget(rb2.widget)

        # Test setting rb1 to rb2
        prop = rb2.get_prop('group')
        command_manager.execute(CommandSetProperty(prop, rb1.widget),
                                self.project)

        group = rb2.widget.get_group()
        assert len(group) == 2
        assert rb1.widget in group
        assert rb2.widget in group

        # Test setting rb1 to rb2 again (this shouldn't crash)
        prop = rb2.get_prop('group')
        command_manager.execute(CommandSetProperty(prop, rb1.widget),
                                self.project)

        # Test setting rb2 to None (should remove it from any groups)
        prop = rb2.get_prop('group')
        command_manager.execute(CommandSetProperty(prop, None),
                                self.project)

        assert prop.get() == None
        group = rb2.widget.get_group()
        assert len(group) == 1
        assert rb2.widget in group

    def testGroupChange(self):
        "Test for bug #172373"
        vbox = self.add_child(self.window, 'GtkVBox',
                               self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        rb1 = self.add_child(vbox, 'GtkRadioButton',
                             vbox.widget.get_children()[0])
        self.project.add_widget(rb1.widget)
        rb2 = self.add_child(vbox, 'GtkRadioButton',
                             vbox.widget.get_children()[1])
        self.project.add_widget(rb2.widget)
        rb2.get_prop('group').set(rb1.widget)

        self.remove_child(vbox, rb1)
        self.project.remove_widget(rb1.widget)

        rb2.select()


    def testGroupPropUndo(self):
        # test for #336911, #342302
        vbox = self.add_child(self.window, 'GtkVBox',
                               self.window.widget.get_child())
        self.project.add_widget(vbox.widget)
        rb1 = self.add_child(vbox, 'GtkRadioButton',
                             vbox.widget.get_children()[0])
        self.project.add_widget(rb1.widget)
        rb2 = self.add_child(vbox, 'GtkRadioButton',
                             vbox.widget.get_children()[1])
        self.project.add_widget(rb2.widget)

        prop = rb2.get_prop('group')
        command_manager.execute(CommandSetProperty(prop, rb1.widget),
                                self.project)
        assert prop.get() == rb1.widget

        command_manager.undo(self.project)
        assert prop.get() == None

        command_manager.redo(self.project)
        assert prop.get() == rb1.widget
