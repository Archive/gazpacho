import gobject
import gtk

from kiwi.utils import gsignal

class IUndoTask:
    def undo():
        pass

    def redo():
        pass

def get_child_properties(widget):
    parent = widget.parent
    prop_state = {}
    for pspec in gtk.container_class_list_child_properties(parent):
        if pspec.flags & gobject.PARAM_READABLE:
            value = parent.child_get_property(widget, pspec.name)
            prop_state[pspec.name] = value
    return prop_state

class UndoPropertyTask:
    # implements(IUndoTask):
    def __init__(self, gobj, name, old, child=False):
        self._gobj = gobj
        self._name = name
        self._old = old

        if child:
            new = gobj.parent.child_get_property(gobj, name)
        else:
            new = gobj.get_property(name)

        self._new = new
        self._child = child
        self._action = self._find_action(gobj, name)
        self._state = {}

    def __repr__(self):
        return '<UndoPropertyTask %s.%s>' % (
            gobject.type_name(self._gobj), self._name)

    def _find_action(self, gobj, name):
        type_name = gobject.type_name(gobj)
        while True:
            try:
                gtype = gobject.type_parent(type_name)
                type_name = gobject.type_name(gtype)
            except RuntimeError:
                return self._default_action

            action = getattr(self, 'action_%s_%s' % (type_name, name), None)
            if action:
                return action

        raise AssertionError

    def _default_action(self, gobj, value):
        if self._child:
            self._gobj.parent.child_set_property(gobj, self._name, value)
        else:
            self._gobj.set_property(self._name, value)

    def action_GtkWidget_parent(self, widget, value):
        if value is None:
            self._state = get_child_properties(widget)
            widget.parent.remove(widget)
        else:
            parent = value
            parent.add(widget)
            for propname, value in self._state.items():
                parent.child_set_property(widget, propname, value)

    # IUndoTask

    def undo(self):
        self._action(self._gobj, self._old)

    def redo(self):
        self._action(self._gobj, self._new)

class UndoTransaction(object):
    def __init__(self, description):
        self.description = description
        self._tasks = []
        self._listened_objects = {}
        self._finished = False

    def __repr__(self):
        tasks = len(self._tasks)
        if tasks == 1:
            desc = '1 task'
        else:
            desc = '%d tasks' % tasks
        return '<UndoTransaction "%s" (%s)>' % (
            self.description, desc)

    def __iter__(self):
        return iter(self._tasks)

    def _on_notify(self, gobj, pspec, state, child=False):
        task = UndoPropertyTask(gobj, pspec.name, state[pspec.name], child)
        self._tasks.append(task)

    def listen(self, gobj):
        prop_state = {}
        for pspec in gobj.props:
            if pspec.flags & gobject.PARAM_READABLE:
                prop_state[pspec.name] = getattr(gobj.props, pspec.name)

        signal_ids = []
        signal_ids.append(gobj.connect(
            'notify', self._on_notify, prop_state))
        self._listened_objects[gobj] = signal_ids

        # FIXME: gobj.parent is added to avoid getting too many changes, but
        # it might be needed in some cases, find out when.
        if isinstance(gobj, gtk.Widget) and gobj.parent:
            prop_state = get_child_properties(gobj)
            signal_ids.append(gobj.connect(
                'child-notify', self._on_notify, prop_state, True))

    def unlisten(self, gobj):
        for signal_id in self._listened_objects[gobj]:
            gobj.disconnect(signal_id)
        del self._listened_objects[gobj]

    def finish(self):
        if self._finished:
            raise TypeError("finished() already called on %s" % self)

        for gobj in self._listened_objects.keys():
            self.unlisten(gobj)
        self._finished = True

    def undo(self):
        for task in self._tasks[::-1]:
            task.undo()

    def redo(self):
        for task in self._tasks:
            task.redo()

    def tasks(self):
        return self._tasks

class UndoManager(gobject.GObject):
    gsignal('changed')
    def __init__(self):
        gobject.GObject.__init__(self)

        self._transactions = []
        self._current = 0

    def __len__(self):
        return len(self._transactions)

    def __iter__(self):
        return iter(self._transactions)

    def new(self, description):
        transactions = self._transactions

        transactions = transactions[:self._current]

        transaction = UndoTransaction(description)
        transactions.append(transaction)
        self._transactions = transactions

        self._current = len(self)
        self.emit('changed')

        return transaction

    def current(self):
        return self._transactions[self._current]

    def can_undo(self):
        return self._current > 0

    def can_redo(self):
        return self._current < len(self)

    def is_current(self, transaction):
        return self._current == self._transactions.index(transaction)

    def on_top(self):
        return self._current == len(self)

    def undo(self):
        if not self.can_undo():
            raise Exception

        self._current -= 1

        transaction = self.current()
        transaction.undo()
        self.emit('changed')

    def redo(self):
        if not self.can_redo():
            raise Exception

        transaction = self.current()
        transaction.redo()

        self._current += 1
        self.emit('changed')

def undo_cb(button, manager):
    manager.undo()

def redo_cb(button, manager):
    manager.redo()

def dump_cb(button, manager):
    print '==[ DUMP ]=='
    for trans in manager:
        if manager.is_current(trans):
            prefix = '->'
        else:
            prefix = '  '

        print prefix, trans
        for task in trans:
            print '    ', task

    if manager.on_top():
        print '->'

def manager_changed_cb(manager, undo, redo):
    undo.set_sensitive(manager.can_undo())
    redo.set_sensitive(manager.can_redo())

def test_label(main_vbox, manager):
    label = gtk.Label('Label')
    label.show()

    undo = manager.new('Adding label')

    undo.listen(label)
    main_vbox.pack_end(label)

    undo.finish()

    undo = manager.new('Remove label')

    undo.listen(label)
    main_vbox.remove(label)

    undo.finish()

def test_vbox(main_vbox, manager):

    undo = manager.new('Adding vbox & children')

    vbox = gtk.VBox()
    vbox.show()

    undo.listen(vbox)
    main_vbox.pack_start(vbox)

    for i in range(3):

        button = gtk.Button()
        button.show()

        undo.listen(button)
        vbox.pack_start(button)
        button.set_label('Test %d' % (9 + i+1))

    undo.finish()

    undo = manager.new('Setting child packing')

    undo.listen(button)
    vbox.set_child_packing(button, True, True, 10, gtk.PACK_END)

    undo.finish()


def test_table(main_vbox, manager):

    undo = manager.new('Adding table')

    table = gtk.Table()
    undo.listen(table)
    main_vbox.pack_start(table)

    undo.finish()

    for x in range(3):
        for y in range(3):
            undo = manager.new('Adding button to table')

            button = gtk.Button()
            undo.listen(button)
            table.attach(button, x, x+1, y, y+1)
            button.set_label('Test %d' % ((x)*3+(y+1)))

            undo.finish()

def test():
    window = gtk.Window()
    window.connect('delete-event', gtk.main_quit)

    main_vbox = gtk.VBox()
    window.add(main_vbox)

    manager = UndoManager()

    test_label(main_vbox, manager)
    test_table(main_vbox, manager)
    test_vbox(main_vbox, manager)

    hbox = gtk.HBox()
    main_vbox.pack_end(hbox, False, False)

    button = gtk.Button('Undo')
    button.connect('clicked', undo_cb, manager)
    hbox.pack_start(button)
    undo_button = button

    button = gtk.Button('Redo')
    button.set_sensitive(False)
    button.connect('clicked', redo_cb, manager)
    hbox.pack_start(button)
    redo_button = button

    button = gtk.Button('Dump')
    button.connect('clicked', dump_cb, manager)
    hbox.pack_start(button)

    manager.connect('changed', manager_changed_cb,
                    undo_button, redo_button)

    window.set_size_request(200, -1)
    window.show_all()
    gtk.main()

test()
