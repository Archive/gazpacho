# Copyright (C) 2004,2005 by SICEm S.L.
# Copyright (C) 2005 Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

import gettext
import optparse
import os
import sys
import gobject
import gtk

_ = gettext.gettext

PYGTK_REQUIRED = (2, 6, 0)

class WorkTableChild:
    """Attributes of a child widget stored in WorkTable"""

    DECORATION_PAD = 5

    def __init__(self, widget):
        self.widget = widget
        self.x = 0
        self.y = 0
        self.window_x = 0
        self.window_y = 0
        self.layout = None
        self.title_height = 0
        self.decoration_x = 0
        self.decoration_y = 0
        self.decoration_width = 1
        self.decoration_height = 1
        
    def set_title(self, title):
        self.layout = self.widget.get_parent().create_pango_layout(title)
        extents = self.layout.get_pixel_extents()
        self.title_height = extents[1][3]

    def compute_size(self):
        parent = self.widget.get_parent()
        self.window_x = self.x + parent.allocation.x + parent.border_width
        self.window_y = self.y + parent.allocation.y + parent.border_width
        self.decoration_x = self.window_x - self.DECORATION_PAD
        self.decoration_y = self.window_y - self.DECORATION_PAD*2 - self.title_height
        req = self.widget.size_request()
        self.decoration_width = req[0] + self.DECORATION_PAD*2
        self.decoration_height = req[1] + self.DECORATION_PAD*2 + self.title_height + self.DECORATION_PAD

    def window_point_in_decoration(self, x, y):
        if y < self.decoration_y:
            return False
        elif y > self.decoration_y + self.decoration_height:
            return False
        elif x < self.decoration_x:
            return False
        elif x > self.decoration_x + self.decoration_width:
            return False
        elif y < self.window_y:
            return True
        elif x < self.decoration_x + self.DECORATION_PAD:
            return True
        elif x > self.decoration_x + self.decoration_width - self.DECORATION_PAD:
            return True
        elif y > self.decoration_y + self.decoration_height - self.DECORATION_PAD:
            return True
        else:
            return False

### FIXME the involvement of GtkFixed is a lame hack
### because you can't override forall in python at the moment (right?)    
class WorkTable(gtk.Fixed): ## FIXME derive from Container
    __gsignals__ = {
        'expose_event': 'override',
        'realize' : 'override',
        'size_request': 'override',
        'size_allocate': 'override',
        'add' : 'override',
        'remove' : 'override',
        'motion_notify_event' : 'override',
        'button_press_event' : 'override',
        'button_release_event' : 'override',
        'key_press_event' : 'override'
    }
    
    def __init__(self):
        #gtk.Container.__init__(self)
        gtk.Fixed.__init__(self) #FIXME
        self.children = []

        # if going Fixed -> Container we need to create a
        # window in realized
        self.unset_flags(gtk.NO_WINDOW)
        self.add_events(gtk.gdk.BUTTON_PRESS_MASK)
        
        self.moving_child = None
        self.moving_start_x_pointer = 0
        self.moving_start_y_pointer = 0
        self.moving_start_x_position = 0
        self.moving_start_y_position = 0

    def set_child_position(self, widget, x, y):
        for c in self.children:
            if c.widget == widget:
                if c.x != x or c.y != y:
                    c.x = x
                    c.y = y
                    if c.widget.flags() & gtk.VISIBLE:
                        self.queue_resize()
                return

    def set_child_title(self, widget, title):
        for c in self.children:
            if c.widget == widget:
                c.set_title(title)
                if c.widget.flags() & gtk.VISIBLE:
                    self.queue_resize()
                return

    def _pick_child(self, window_x, window_y):
        reversed = list(self.children)
        reversed.reverse()
        for c in reversed:
            if c.window_point_in_decoration(window_x, window_y):
                return c
        return None

    def _begin_move_child(self, child, x, y, time):
        if self.moving_child != None:
            raise "can't move two children at once"

        if gtk.gdk.pointer_grab(self.window,
                                False,
                                gtk.gdk.BUTTON_RELEASE_MASK |
                                gtk.gdk.BUTTON_RELEASE_MASK |
                                gtk.gdk.POINTER_MOTION_MASK,
                                None, None, long(time)) != gtk.gdk.GRAB_SUCCESS:
            print "grab failed"
            return

        self.children.remove(child)
        self.children.append(child)
        
        self.moving_child = child
        self.moving_start_x_pointer = x
        self.moving_start_y_pointer = y
        self.moving_start_x_position = child.x
        self.moving_start_y_position = child.y

    def _update_move_child (self, x, y):
        if not self.moving_child:
            return
        
        new_x = self.moving_start_x_position + (x - self.moving_start_x_pointer)
        new_y = self.moving_start_y_position + (y - self.moving_start_y_pointer)

        self.set_child_position(self.moving_child.widget, new_x, new_y)

    def _end_move_child(self, time):
        if not self.moving_child:
            return

        gtk.gdk.pointer_ungrab(long(time))
        self.moving_child = None

    def do_button_press_event(self, event):
        if self.moving_child != None:
            return

        c = self._pick_child(int(event.x), int(event.y))
        if c == None:
            return

        self._begin_move_child(c, int(event.x), int(event.y), event.time)

    def do_button_release_event(self, event):
        self._update_move_child(int(event.x), int(event.y))
        self._end_move_child(event.time)
    
    def do_motion_notify_event(self, event):
        if self.moving_child != None:
            self._update_move_child(int(event.x), int(event.y))

    def do_key_press_event(self, event):
        if event.keyval == gtk.keysyms.Escape:
            self._end_move_child(event.time)

    def do_add(self, widget):
        child = WorkTableChild(widget)
        self.children.append(child)
        #widget.set_parent(self)
        gtk.Fixed.do_add(self,widget) #FIXME

        # default title
        title = type(child.widget).__name__
        child.set_title(title)

    def do_remove(self, widget):
        if self.moving_child and self.moving_child.widget == widget:
            self._end_move_child(0)
        
        for c in self.children:
            if c.widget == widget:
                was_visible = c.widget.flags() & gtk.VISIBLE
                self.children.remove(c)
                #c.widget.unparent()
                gtk.Fixed.remove(self,c.widget) # FIXME
                if was_visible:
                    self.queue_resize()
                return

    def do_realize(self):
        #gtk.Container.do_realize(self)
        gtk.Fixed.do_realize(self)

    def do_expose_event(self, event):
        # draw background
        event.window.draw_rectangle(self.style.base_gc[self.state], True,
                                    self.allocation.x,
                                    self.allocation.y,
                                    self.allocation.width,
                                    self.allocation.height)

        # draw children
        gtk.Container.do_expose_event(self, event)

        # draw little frame thingies
        gc = gtk.gdk.GC(event.window)
        gc.set_rgb_fg_color(self.style.text[self.state])

        for c in self.children:

            event.window.draw_rectangle(gc, False,
                                        c.decoration_x, c.decoration_y,
                                        c.decoration_width, c.decoration_height)

            event.window.draw_line(gc,
                                   c.decoration_x,
                                   c.window_y - c.DECORATION_PAD,
                                   c.decoration_x + c.decoration_width,
                                   c.window_y - c.DECORATION_PAD)

            event.window.draw_layout(gc,
                                     c.decoration_x + c.DECORATION_PAD,
                                     c.decoration_y + c.DECORATION_PAD,
                                     c.layout)

    def do_size_request(self, requisition):
        requisition.width = 0
        requisition.height = 0

        for c in self.children:
            if c.widget.flags() & gtk.VISIBLE:
                c.compute_size()
                requisition.width = max(requisition.width, c.decoration_x + c.decoration_width)
                requisition.height = max(requisition.height, c.decoration_y + c.decoration_height)

        requisition.width = requisition.width + self.border_width * 2
        requisition.height = requisition.height + self.border_width * 2

    def do_size_allocate(self, allocation):
        self.allocation = allocation
        border_width = self.border_width
        for c in self.children:
            if c.widget.flags() & gtk.VISIBLE:
                child_req = c.widget.get_child_requisition()
                w = child_req[0]
                h = child_req[1]
                c.widget.size_allocate((c.window_x, c.window_y, w, h))

gobject.type_register(WorkTable)        

class EmbeddableMessageDialog(gtk.MessageDialog):
    """Message dialog with crazy hacks so you can embed it as a non-toplevel"""
    ### FIXME this class has to be generalized to all kinds of toplevel window,
    ### which should be trivial
    ### A better approach might be to do this entirely via signal connection rather
    ### than via subclass. Then you could have a make_embeddable() function
    ### that took any toplevel.
    
    __gsignals__ = {
        'realize': 'override',
        'size-allocate' : 'override',
        'show' : 'override',
        'hide' : 'override',
        'map' : 'override',
        'unmap' : 'override'
        }

    def __init__(self):
        gobject.GObject.__gobject_init__(self, message_type=gtk.MESSAGE_INFO,
                                         buttons=gtk.BUTTONS_OK)
        self.label.set_text('Hello')
                                   
        self.unset_flags (gtk.TOPLEVEL)

    def do_realize(self):
        self.set_flags(self.flags() | gtk.REALIZED)

        events = (gtk.gdk.EXPOSURE_MASK |
                  gtk.gdk.KEY_PRESS_MASK |
                  gtk.gdk.KEY_RELEASE_MASK |
                  gtk.gdk.ENTER_NOTIFY_MASK |
                  gtk.gdk.LEAVE_NOTIFY_MASK |
                  gtk.gdk.STRUCTURE_MASK)
        
        self.window = gtk.gdk.Window(self.get_parent_window(),
                                     window_type=gtk.gdk.WINDOW_CHILD,
                                     x=self.allocation.x,
                                     y=self.allocation.y,
                                     width=self.allocation.width,
                                     height=self.allocation.height,
                                     wclass=gtk.gdk.INPUT_OUTPUT,
                                     visual=self.get_visual(),
                                     colormap=self.get_colormap(),
                                     event_mask=self.get_events() | events)
        
        self.window.set_user_data(self)
        self.style.attach(self.window)
        self.style.set_background(self.window, gtk.STATE_NORMAL)

        self.window.enable_synchronized_configure()

    def do_size_allocate(self, allocation):
        self.allocation = allocation
        if self.flags() & gtk.REALIZED:
            self.window.move_resize(*allocation)

        if self.get_child() and (self.get_child().flags() & gtk.VISIBLE):
            child_allocation = (self.border_width, self.border_width,
                                max (1, self.allocation.width - self.border_width * 2),
                                max (1, self.allocation.height - self.border_width * 2))
            self.get_child().size_allocate (child_allocation)

    def do_show(self):
        gtk.Bin.do_show(self)
        
    def do_hide(self):
        gtk.Bin.do_hide(self)

    def do_map(self):
        gtk.Bin.do_map(self)
        
    def do_unmap(self):
        gtk.Bin.do_unmap(self)    

gobject.type_register(EmbeddableMessageDialog)

def show_debug():
    print 'Loading PyGTK...'
    import gobject
    import gtk
    if (os.path.dirname(os.path.dirname(gtk.__file__)) !=
        os.path.dirname(gobject.__file__)):
        print ('WARNING: Gtk and GObject modules are not loaded from '
               'the same prefix')
        
    print 'Python:\t', '.'.join(map(str, sys.version_info[:3]))
    print 'GTK:\t', '.'.join(map(str, gtk.gtk_version))
    print 'PyGTK:\t', '.'.join(map(str, gtk.pygtk_version))
    
def launch(filenames=[], debug=False, profile=False, batch=None):
    if debug:
        show_debug()

    if gtk.pygtk_version < PYGTK_REQUIRED:
        raise SystemExit("PyGTK 2.6.0 or higher required to run Gazpacho")

    # Ignore deprecation warnings when using 2.7.0
    if gtk.pygtk_version >= (2, 7, 0):
        if gtk.pygtk_version in ((2, 7, 0), (2, 7, 1)):
            raise SystemExit("PyGtk 2.7.0 and 2.7.1 are buggy "
                             "Upgrade to 2.7.2 or downgrade to 2.6.x")
        
        if debug:
            print 'Using PyGTK 2.7.x, ignoring deprecation warnings'
        import warnings
        warnings.filterwarnings('ignore', category=DeprecationWarning)

    window = gtk.Window()
    window.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse("white"))

    sw = gtk.ScrolledWindow()
    sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    window.add(sw)

    table = WorkTable()
    table.set_border_width(20)
    
    sw.add_with_viewport(table)

    dialog = EmbeddableMessageDialog()

    table.add(dialog)
    table.set_child_position(dialog, 40, 40)

    button = gtk.Button("A Button")
    table.add (button)
    table.set_child_position(button, 200, 200)

    table.show_all()
    req = table.size_request()
    window.set_default_size(req[0] + 10, req[1] + 10)
    window.show_all()

    gtk.main()

def main(args=[]):
        
    filenames = []

    launch(filenames, None, None, None)


main()
