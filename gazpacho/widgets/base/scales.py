# Copyright (C) 2005 by Async Open Source
#               2007 by Mauricio Vieira 
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

from gazpacho.properties import prop_registry, \
     ProxiedProperty, AdjustmentType, FloatType, IntType

class ScaleAdjustmentType(AdjustmentType):
    def __init__(self, gobj):
        super(AdjustmentType, self).__init__(gobj)
        # Initialize us to the value fetch from the loader
        self._value = gobj.widget.get_adjustment()

prop_registry.override_simple('GtkHScale::adjustment', ScaleAdjustmentType)
prop_registry.override_simple('GtkVScale::adjustment', ScaleAdjustmentType)


class AdjustmentProxy(ProxiedProperty):
    def get_target(self):
        return self.object.get_adjustment()

    def save(self):
        # Not saving, they're saved in GtkVScale::adjustment instead
        return

class HLower(AdjustmentProxy, FloatType):
    label = "Min"
    default = 0
    target_name = 'lower'
    tooltip = "The minimum value the hscale can have"
prop_registry.override_property('GtkHScale::lower', HLower)

class VLower(AdjustmentProxy, FloatType):
    label = "Min"
    default = 0
    target_name = 'lower'
    tooltip = "The minimum value the vscale can have"
prop_registry.override_property('GtkVScale::lower', VLower)

class HUpper(AdjustmentProxy, FloatType):
    label = "Max"
    default = 100
    target_name = 'upper'
    tooltip = "The maximum value the hscale can have"
prop_registry.override_property('GtkHScale::upper', HUpper)

class VUpper(AdjustmentProxy, FloatType):
    label = "Max"
    default = 100
    target_name = 'upper'
    tooltip = "The maximum value the vscale can have"
prop_registry.override_property('GtkVScale::upper', VUpper)

class HStep(AdjustmentProxy, IntType):
    label = "Step increment"
    default = 1
    tooltip = "Increment applied for each left mousebutton press"
    target_name = 'step-increment'
prop_registry.override_property('GtkHScale::step-increment', HStep)

class VStep(AdjustmentProxy, IntType):
    label = "Step increment"
    default = 1
    tooltip = "Increment applied for each left mousebutton press"
    target_name = 'step-increment'
prop_registry.override_property('GtkVScale::step-increment', VStep)

class HPage(AdjustmentProxy, IntType):
    label = "Page increment"
    default = 10
    tooltip = "Increment applied for each middle mousebutton press"
    target_name = 'page-increment'
prop_registry.override_property('GtkHScale::page-increment', HPage)

class VPage(AdjustmentProxy, IntType):
    label = "Page increment"
    default = 10
    tooltip = "Increment applied for each middle mousebutton press"
    target_name = 'page-increment'
prop_registry.override_property('GtkVScale::page-increment', VPage)
