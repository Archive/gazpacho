HOSTNAME=$1
SRCDIR=$(pwd)
DISTDIR=$(pwd)/dist

VERSION=$(grep __version__ $SRCDIR/gazpacho/__init__.py|cut -d\" -f2)

echo "Copying tarballs for version $VERSION"
scp $DISTDIR/gazpacho-$VERSION.tar.gz \
  $DISTDIR/gazpacho-$VERSION.zip \
  $DISTDIR/gazpacho-$VERSION.win32.exe $HOSTNAME:

echo "Installing version $VERSION"
ssh $HOSTNAME ./install-gazpacho $VERSION



