#!/bin/sh

SRCDIR=$(pwd)
BUILDDIR=/tmp/gaz-rel/
INSTDIR=/tmp/gaz-inst/

VERSION=$(grep __version__ $SRCDIR/gazpacho/__init__.py|cut -d\" -f2)
PACKAGE=$SRCDIR/dist/gazpacho-$VERSION.tar.gz

die() {
    exit 1
}

echo "Creating source dist"

cd $SRCDIR
rm -f MANIFEST
rm -fr dist/gazpacho-$VERSION*
echo "Building source distributions"
python setup.py -q sdist --formats gztar,zip || die
python setup.py -q bdist_wininst || die

echo "Extracting source dist"
rm -fr $BUILDDIR
mkdir $BUILDDIR
cd $BUILDDIR
tar xfz $PACKAGE 

cd gazpacho-$VERSION
echo "Installing dist"
rm -fr $INSTDIR
python setup.py -q install --prefix $INSTDIR

echo "Running installed"
$INSTDIR/bin/gazpacho -u $INSTDIR/share/doc/gazpacho/examples/uimanager.glade

echo "Cleaning up"
rm -fr $INSTDIR
rm -fr $BUILDDIR



